﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Web.Models
{
    public class GalleryViewModel
    {
        public IImage[] Images { get; set; }

        public GalleryFilterModel Filter { get; set; }

        public IEnumerable<string> MostlyUsedTags { get; set; }

        public bool HasNext { get; set; }

        public bool HasPrevious { get; set; }
    }
}