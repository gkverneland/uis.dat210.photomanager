﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Web.Models
{
    public class SystemViewModel
    {
        public IIndexingService IndexingService { get; set; }

        public string PhotoLibraryPath { get; set; }
        public string ThumbnailPath { get; set; }
    }
}