﻿using System;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Web.Models
{
    public class GalleryFilterModel : IImageFilter
    {
        public GalleryFilterModel()
        {
            Order = ImageOrder.DateTaken | ImageOrder.Descending;
            PageNumber = 1;
            PageSize = 520;
        }

        public string Tag { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public ImageOrder Order { get; set; }

        public int PageSize { get; set; }

        public int PageNumber { get; set; }

        public int? MinimumRating { get; set; }

        public bool GroupByDate { get; set; }

        int? IImageFilter.PageSize { get { return PageSize; } }
        int? IImageFilter.PageNumber { get { return PageNumber; } }
    }
}