﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Web.Models
{
    public class ImageViewModel
    {
        public IImage Image { get; set; }
    }
}