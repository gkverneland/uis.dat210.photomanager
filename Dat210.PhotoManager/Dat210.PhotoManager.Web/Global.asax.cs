﻿using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using log4net;
using Ninject;
using Ninject.Web.Common;

namespace Dat210.PhotoManager.Web
{
    public class MvcApplication : NinjectHttpApplication
    { 
        private static readonly ILog Log = LogManager.GetLogger(typeof(MvcApplication));

        protected override void OnApplicationStarted()
        {
            log4net.Config.XmlConfigurator.Configure();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Log.Info("Started application");
        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            kernel.Load(typeof (IndexingService).Assembly);
            kernel.Get<IIndexingService>(); //instantiate
            return kernel;
        }
    }
}
