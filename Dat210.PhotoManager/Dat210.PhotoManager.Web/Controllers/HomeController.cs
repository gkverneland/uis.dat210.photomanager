﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Web.Models;
using log4net;

namespace Dat210.PhotoManager.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IImageLibrary _library;

        public HomeController(IImageLibrary library)
        {
            _library = library;
        }

        public ActionResult Index(GalleryFilterModel filterModel)
        {
            var viewModel = new GalleryViewModel();
            viewModel.Images = _library.GetImageInfo(filterModel).ToArray();
            viewModel.Filter = filterModel;
            viewModel.HasNext = viewModel.Images.Any() && _library.GetNextImageId(viewModel.Images.Last().Id, filterModel).HasValue;
            viewModel.HasPrevious = viewModel.Images.Any() && _library.GetPreviousImageId(viewModel.Images.First().Id, filterModel).HasValue;
            viewModel.MostlyUsedTags = _library.GetMostlyUsedTags(50);
            Session["filter"] = filterModel;
            return View(viewModel);
        }
    }
}
