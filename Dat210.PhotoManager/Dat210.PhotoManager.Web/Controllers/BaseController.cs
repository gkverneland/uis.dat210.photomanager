﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Dat210.PhotoManager.Web.Controllers
{
    public class BaseController : Controller
    {
        private static readonly CultureInfo Norwegian = new CultureInfo("nb-NO");
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = Norwegian;
            base.OnActionExecuting(filterContext);
        }
    }
}