﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Web.Models;

namespace Dat210.PhotoManager.Web.Controllers
{
    public class SystemController : BaseController
    {
        private readonly IIndexingService _service;
        private readonly IApplicationConfiguration _configuration;

        public SystemController(IIndexingService service, IApplicationConfiguration configuration)
        {
            _service = service;
            _configuration = configuration;
        }

        //
        // GET: /System/
        public ActionResult Index()
        {
            return View(new SystemViewModel
            {
                IndexingService = _service, 
                PhotoLibraryPath = _configuration.GetPhotoLibraryPath(), 
                ThumbnailPath = _configuration.GetThumbnailPath()
            });
        }

        public ActionResult SetPhotoLibraryPath(string photoLibraryPath)
        {
            _configuration.SetPhotoLibraryPath(photoLibraryPath);
            _service.Add();
            return RedirectToAction("Index");
        }

        public ActionResult SetThumbnailPath(string thumbnailPath)
        {
            _configuration.SetThumbnailPath(thumbnailPath);
            return RedirectToAction("Index");
        }
	}
}