﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Web.Models;
using log4net;

namespace Dat210.PhotoManager.Web.Controllers
{
    public class ImageController : Controller
    {
        private readonly IImageLibrary _library;
        private static readonly ILog Log = LogManager.GetLogger(typeof(ImageController));

        public ImageController(IImageLibrary imageLibrary)
        {
            _library = imageLibrary;
        }

        public ActionResult View(Guid id)
        {
            var model = new ImageViewModel();
            model.Image = _library.GetImageInfo(id);
            return View(model);
        }
        
        public ActionResult Next(Guid id)
        {
            var nextImage = _library.GetNextImageId(id, Session["filter"] as IImageFilter ?? new GalleryFilterModel());
            return RedirectToAction("View", new {id = nextImage.GetValueOrDefault(id)});
        }

        public ActionResult Previous(Guid id)
        {
            var nextImage = _library.GetPreviousImageId(id, Session["filter"] as IImageFilter ?? new GalleryFilterModel());
            return RedirectToAction("View", new { id = nextImage.GetValueOrDefault(id) });
        }

        public ActionResult SmallThumbnail(Guid id, string hash)
        {
            return GetImageCahed(id, hash, () => _library.GetSmallThumbnail(id));
        }

        public ActionResult Preview(Guid id, string hash)
        {
            return GetImageCahed(id, hash, () => _library.GetPreview(id));
        }

        public ActionResult Image(Guid id, string hash)
        {
            return GetImageCahed(id, hash, () => _library.GetImage(id));
        }

        private ActionResult GetImageCahed(Guid id, string hash, Func<IFile> getFile)
        {
            var image = _library.GetImageInfo(id);
            if (String.IsNullOrEmpty(Request.Headers["If-Modified-Since"]) || 
                String.IsNullOrEmpty(hash) || hash != image.Hash)
            {
                Response.Cache.SetCacheability(HttpCacheability.Public);
                Response.Cache.SetMaxAge(TimeSpan.FromDays(30));
                Response.Cache.SetExpires(DateTime.Now.AddDays(30));
                Response.Cache.SetLastModified(image.Modified);
                var file = getFile();
                return File(file.Data, file.ContentType);
            }
            return new HttpStatusCodeResult(304, "Not Modified");
        }

        [HttpPost]
        public ActionResult AddTag(Guid id, string tag, string returnUrl)
        {
            if (!String.IsNullOrEmpty(tag))
            {
                var image = _library.GetImageInfo(id);
                if (image.Tags.All(t => !t.Equals(tag, StringComparison.InvariantCultureIgnoreCase)))
                {
                    Log.Info("Adding tag " + tag + " to image " + image.OriginalFilePath);
                    image.Tags.Add(tag);
                    _library.SaveImage(image);
                }
            }

            if (!String.IsNullOrEmpty(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("View", "Image", new {id = id});
        }

        public ActionResult RemoveTag(Guid id, string tag, string returnUrl)
        {
            if (!String.IsNullOrEmpty(tag))
            {
                var image = _library.GetImageInfo(id);
                if (image.Tags.Any(t => t.Equals(tag, StringComparison.InvariantCultureIgnoreCase)))
                {
                    Log.Info("Removing tag " + tag + " from image " + image.OriginalFilePath);
                    image.Tags.Remove(tag);
                    _library.SaveImage(image);
                }
            } 


            if (!String.IsNullOrEmpty(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("View", "Image", new { id = id });
        }

        public ActionResult SetRating(Guid id, int rating, string returnUrl)
        {
            var image = _library.GetImageInfo(id);
            if (image.Rating != rating)
            {
                Log.Info("Setting rating to " + rating + " for image " + image.OriginalFilePath);
                image.Rating = rating;
                _library.SaveImage(image);
            }
            
            if (!String.IsNullOrEmpty(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("View", "Image", new { id = id });
        }

        public ActionResult Comment(Guid id, string comment, string returnUrl)
        {
            var image = _library.GetImageInfo(id);
            if (image.Comment != comment)
            {
                Log.Info("Setting comment for image " + image.OriginalFilePath + Environment.NewLine + comment);
                image.Comment = comment;
                _library.SaveImage(image);
            }

            if (!String.IsNullOrEmpty(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("View", "Image", new {id = id});
        }

        public ActionResult Rotate(Guid id, bool counterClockwise, string returnUrl)
        {
            Log.Info("Rotating image " + (counterClockwise ? "counter" : "") + "clockwise " + id);
            _library.RotateImage(id, counterClockwise);
            if (!String.IsNullOrEmpty(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("View", "Image", new {id = id});
        }

        public ActionResult Delete(Guid id, string returnUrl)
        {
            var next = Next(id);
            _library.DeleteImage(id);
            return next;
        }
	}
}