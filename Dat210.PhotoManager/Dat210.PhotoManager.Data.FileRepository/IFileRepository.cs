﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Data.FileRepository
{
    public interface IFileRepository
    {
        IEnumerable<string> ListImageFiles(string directory, bool recursive);
        IFileInformation GetImageFile(string path);
        IFile ReadImageFile(string path);
        string GetFileHash(byte[] file);
        string SaveThumbnail(byte[] file, string extension);
        void SaveImage(byte[] image, string path);
        bool FileExists(string path);
        void Delete(string path);
        /// <summary>
        /// Ensures a file is accessible and returns an object that can be locked on to ensure no other threads
        /// access the same file
        /// </summary>
        object EnsureFileIsAccessible(string path, TimeSpan timeout);
    }
}
