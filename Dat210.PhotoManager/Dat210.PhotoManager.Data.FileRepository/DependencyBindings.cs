﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

namespace Dat210.PhotoManager.Data.FileRepository
{
    public class DependencyBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IFileRepository>().To<FileSystemRepository>().InSingletonScope();
        }
    }
}
