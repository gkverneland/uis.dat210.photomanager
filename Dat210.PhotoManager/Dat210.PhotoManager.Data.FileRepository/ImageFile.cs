﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Data.FileRepository
{
    internal class ImageFile : IFile
    {
        public byte[] Data { get; set; }

        public string ContentType { get; set; }
    }
}
