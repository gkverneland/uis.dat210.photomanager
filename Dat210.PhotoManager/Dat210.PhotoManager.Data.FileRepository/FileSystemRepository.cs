﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;
using log4net;

namespace Dat210.PhotoManager.Data.FileRepository
{
    internal class FileSystemRepository : IFileRepository
    {
        private readonly string _thumbnailDirectory;
        private static readonly ILog Log = LogManager.GetLogger(typeof(FileSystemRepository));
        public FileSystemRepository(IApplicationConfiguration config)
        {
            _thumbnailDirectory = config.GetThumbnailPath();
            if (!Directory.Exists(_thumbnailDirectory))
                Directory.CreateDirectory(_thumbnailDirectory);
        }

        public IEnumerable<string> ListImageFiles(string directory, bool recursive)
        {
            return Directory.GetFiles(directory, "*.jpg",
                (recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                .Where(p => p != null
                && Path.GetExtension(p).TrimStart('.').Equals("jpg", StringComparison.InvariantCultureIgnoreCase)
                 && !Path.GetFileNameWithoutExtension(p).StartsWith(".", StringComparison.InvariantCultureIgnoreCase)
                    )
                    .OrderByDescending(p => p);
        }

        public string GetFileHash(byte[] file)
        {
            var hashBytes = new SHA1Managed().ComputeHash(file);
            return hashBytes.Aggregate("", (s, b) => s + b.ToString("x2"));
        }

        public IFileInformation GetImageFile(string path)
        {
            if (path == null) throw new ArgumentNullException("path");
            var file = new FileInfo(path);
            return new FileInformation
            {
                Path = path,
                Created = file.CreationTime,
                Modified = file.LastWriteTime,
                Extension = file.Extension.TrimStart('.').ToLower(),
                Name = Path.GetFileNameWithoutExtension(path)
            };
        }

        public IFile ReadImageFile(string path)
        {
            return new ImageFile
            {
                Data = File.ReadAllBytes(path),
                ContentType = "image/" + (Path.GetExtension(path) ?? "jpg").TrimStart('.')
            };
        }

        public string SaveThumbnail(byte[] file, string extension)
        {
            var path = Path.Combine(_thumbnailDirectory, Guid.NewGuid() + "." + extension);
            File.WriteAllBytes(path, file);
            return path;
        }

        public void SaveImage(byte[] image, string path)
        {
            File.WriteAllBytes(path, image);
        }

        public bool FileExists(string path)
        {
            return File.Exists(path);
        }

        public void Delete(string path)
        {
            File.Delete(path);
            Log.Debug("Deleted " + path);
        }

        private readonly Dictionary<string, object> _locks = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
        
        public object EnsureFileIsAccessible(string path, TimeSpan timeout)
        {
            if (!_locks.ContainsKey(path))
                _locks[path] = new object();
            var lockObject = _locks[path];
            lock (lockObject)
            {
                bool accessible = false;
                DateTime start = DateTime.Now;
                while (!accessible)
                {
                    try
                    {
                        if (!FileExists(path))
                            accessible = true;
                        using (File.Open(path, FileMode.Open, FileAccess.Read, FileShare.None))
                            accessible = true;
                    }
                    catch (Exception ex)
                    {
                        TimeSpan duration = (DateTime.Now - start);
                        if (duration > timeout)
                        {
                            throw new Exception("Could not open file, timeout after " + duration.TotalSeconds + " seconds", ex);
                        }
                        Thread.Sleep(500);
                    }
                }
            }

            return lockObject;
        }
    }
}
