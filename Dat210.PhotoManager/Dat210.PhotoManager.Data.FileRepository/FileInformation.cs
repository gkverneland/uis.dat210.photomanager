﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Data.FileRepository
{
    internal class FileInformation : IFileInformation
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}
