using System;

namespace Dat210.FileWatcher
{
    public class FileFailedEventArgs : EventArgs
    {
        public FileFailedEventArgs(string fileName)
        {
            FileName = fileName;
        }

        public string FileName { get; private set; }
    }
}