﻿using System;
using System.IO;

namespace Dat210.FileWatcher
{
    public class FileReadyEventArgs : EventArgs
    {
        public FileReadyEventArgs(string fileName, WatcherChangeTypes change)
        {
            FileName = fileName;
            Change = change;
        }

        public string FileName { get; private set; }
        public WatcherChangeTypes Change { get; set; }
    }
}