﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using log4net;

namespace Dat210.FileWatcher
{
    /// <summary>
    /// Will watch the given folder and notify when a file is created and closed (ready to process)
    /// </summary>
    public class FileGrabber : IDisposable
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(FileGrabber));
        private readonly FileSystemWatcher _watcher;
        private readonly List<string> _filesBeingHandled = new List<string>();
        private bool _isDisposed;
        public TimeSpan RetryTimeout { get; set; }

        public event EventHandler<FileReadyEventArgs> FileReady = delegate { };
        public event EventHandler<FileFailedEventArgs> FileFailed = delegate { };

        public FileGrabber(string folder, string fileFilter, TimeSpan retryTimeout)
        {
            _watcher = new FileSystemWatcher(folder, fileFilter);
            _watcher.IncludeSubdirectories = true;
            _watcher.Created += OnFileChanged;
            _watcher.Changed += OnFileChanged;
            _watcher.Deleted += OnFileChanged;
            _watcher.Renamed += OnFileChanged;
            RetryTimeout = retryTimeout;
        }

        public bool IsEnabled
        {
            get { return _watcher.EnableRaisingEvents; }
            set
            {
                _watcher.EnableRaisingEvents = value;
                if (Log.IsDebugEnabled)
                    Log.Debug("Watching " + _watcher.Filter + " in " + _watcher.Path + " enabled=" + value);
            }
        }

        public bool IncludeSubdirectories
        {
            get { return _watcher.IncludeSubdirectories; }
            set { _watcher.IncludeSubdirectories = value; }
        }

        private void OnFileChanged(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            try
            {
                var sourceFile = fileSystemEventArgs.FullPath;
                if (File.Exists(sourceFile) &&
                    (File.GetAttributes(sourceFile) & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    return;
                }
                
                // put the file name into a list and check for duplicates.
                // this is done since when a file is being created, it might cause several events to be fired.
                // that way we could risk several events queuing up and when the file is closed, all are processed.
                var uniqueName = sourceFile.ToLower() + "_" + fileSystemEventArgs.ChangeType;
                lock (_filesBeingHandled)
                {
                    if (_filesBeingHandled.Contains(uniqueName))
                        return;
                    _filesBeingHandled.Add(uniqueName);
                }
                try
                {
                    var timeout = DateTime.Now.Add(RetryTimeout);
                    for (; ; )
                    {
                        if (_isDisposed) return;

                        if (DateTime.Now > timeout)
                        {
                            FileFailed(this, new FileFailedEventArgs(sourceFile));
                            return;
                        }
                        if (File.Exists(sourceFile) && !FileIsAccessible(sourceFile))
                        {
                            if(Log.IsDebugEnabled)
                                Log.Debug("File not accessible: " + sourceFile);
                            Thread.Sleep(500);
                        }
                        else
                        {
                            FileReady(this, new FileReadyEventArgs(sourceFile, fileSystemEventArgs.ChangeType));
                            return;
                        }
                    }
                }
                finally
                {
                    lock (_filesBeingHandled)
                        _filesBeingHandled.Remove(uniqueName);
                }
            }
            catch (Exception ex)
            {
                Log.Error("FileGrabber failed", ex);
            }
            
        }

        private static bool FileIsAccessible(string filename)
        {
            // If the file can be opened for exclusive access it means that the file
            // is no longer locked by another process.
            try
            {
                using (File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                    return true;
            }
            catch (IOException)
            {
                return false;
            }
        }

        public void Dispose()
        {
            _watcher.Dispose();
            _isDisposed = true;
        }
    }
}