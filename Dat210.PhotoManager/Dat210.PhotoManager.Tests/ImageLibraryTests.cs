﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Data.Indexing.Exceptions;
using Dat210.PhotoManager.Tests.Mocking;
using Dat210.PhotoManager.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace Dat210.PhotoManager.Tests
{
    [TestClass]
    public class ImageLibraryTests
    {
        private readonly IImageLibrary _imageLibrary = Fake.Kernel.Get<IImageLibrary>();
        private readonly IApplicationConfiguration _config = Fake.Kernel.Get<IApplicationConfiguration>();
        private readonly IIndexingService _indexingService = Fake.Kernel.Get<IIndexingService>();
        private readonly IFileRepository _fileRepository = Fake.Kernel.Get<IFileRepository>();

        [TestMethod]
        [DeploymentItem("TestData")]
        public void WhenAddingAnImage_ItShouldShowInGallery()
        {
            var testFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "wallpaper_08.jpg");
            if (!File.Exists(testFile))
                throw new Exception("Test file was not found in " + testFile);
            var path = Path.Combine(_config.GetPhotoLibraryPath(), "TestFile" + Guid.NewGuid().ToString().Replace("-", "") + ".jpg");
            if (File.Exists(path))
            {
                File.Delete(path);
                Console.WriteLine("Deleting " + path);
                _indexingService.WaitUntilIndexed();
                Console.WriteLine("Indexed");
            }
            Console.WriteLine("Copying");
            File.Copy("wallpaper_08.jpg", path);
            Console.WriteLine("Copied");
            if (!File.Exists(path))
                throw new Exception("Test file was not found after moving into photo library");
            try
            {
                _indexingService.Add();
                _indexingService.WaitUntilIndexed();
                Console.WriteLine("Indexed");
                var images = _imageLibrary.GetImageInfo(new ImageFilter());
                var imageCount = images.Count();
                if (imageCount == 0)
                    throw new Exception("No images in library");
                var addedImage =
                    images.FirstOrDefault(
                        i => i.OriginalFilePath.Equals(path, StringComparison.InvariantCultureIgnoreCase));
                if (addedImage == null)
                {
                    foreach (var image in images)
                    {
                        Console.WriteLine(image.OriginalFilePath);
                    }
                    throw new Exception("Was not able to add image file and find it in the library afterwards");
                }
            }
            finally
            {
                _fileRepository.EnsureFileIsAccessible(path, TimeSpan.FromMinutes(1));
                File.Delete(path);
            }
        }

        [TestMethod]
        public void WhenLoadingImages_NoErrorShouldOccur()
        {
            _imageLibrary.GetImageInfo(new ImageFilter());
        }

        [TestMethod]
        public void WhenDeletingAnImage_ItShouldBeRemoved()
        {
            var images = _imageLibrary.GetImageInfo(new ImageFilter());
            if (!images.Any())
                throw new Exception("No images in library");

            var image = images.First();
            var imagePath = image.OriginalFilePath;
            var imageId = image.Id;
            var tempPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            if (File.Exists(tempPath))
                throw new Exception("Temporary file already exists: " + tempPath);
            _fileRepository.EnsureFileIsAccessible(imagePath, TimeSpan.FromSeconds(30));
            File.Copy(imagePath, tempPath);
            File.Delete(imagePath);

            try
            {
                _indexingService.Remove();
                _indexingService.WaitUntilIndexed();
                ImageNotFoundException exception = null;
                try
                {
                    _imageLibrary.GetImageInfo(imageId);
                }
                catch (ImageNotFoundException ex)
                {
                    exception = ex;
                }
                
                Assert.IsTrue(exception != null, "ImageNotFoundException should have been thrown");
            }
            finally
            {
                File.Move(tempPath, imagePath);
            }
        }
    }
}
