﻿using System;
using System.Linq;
using System.Threading;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Tests.Mocking;
using Dat210.PhotoManager.Web;
using Dat210.PhotoManager.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace Dat210.PhotoManager.Tests
{
    [TestClass]
    public class ImageGalleryTests
    {
        [TestInitialize]
        public void Initialize()
        {
            TestData.Copy();
            EnsureImagesInLibrary();
        }

        private readonly IImageLibrary _imageLibrary = Fake.Kernel.Get<IImageLibrary>();

        public void EnsureImagesInLibrary()
        {
            if (!_imageLibrary.GetImageInfo(new GalleryFilterModel { PageSize = 1 }).Any())
                throw new Exception("No images in library");
        }


        [TestMethod]
        public void WhenFilteringByTag_OnlyImagesWithThatTagShouldBeShown()
        {
            var images = _imageLibrary.GetImageInfo(new ImageFilter()).ToList();
            var imageWithTag = images.FirstOrDefault(i => i.Tags.Any());
            if (imageWithTag == null)
            {
                foreach (var image in _imageLibrary.GetImageInfo(new ImageFilter()))
                {
                    Console.WriteLine(image.OriginalFilePath);
                }
                throw new Exception("No images in library has tags");
            }
                
            string tag = imageWithTag.Tags.First();
            var tagImages = _imageLibrary.GetImageInfo(new ImageFilter {Tag = tag});

            Assert.IsTrue(tagImages.Any(), "No images found after applying tag filter");

            Assert.IsTrue(tagImages.All(i => i.Tags.Any(t => t == tag)), "Only images with tag '" + tag + "' should be shown");
        }

        [TestMethod]
        public void WhenFilteringByDate_OnlyImagesWithinTheDateRangeShouldBeShown()
        {
            DateTime from = DateTime.Now.AddYears(-1);
            DateTime to = DateTime.Now.AddMonths(-2);
            var images = _imageLibrary.GetImageInfo(new ImageFilter {FromDate = from, ToDate = to});

            Assert.IsTrue(images.Any(), "No images found within date range " + from.ToShortDateString() + " - " + to.ToShortDateString());

            Assert.IsTrue(images.All(i => i.Taken < to && i.Taken >= from),
                "All images should be within date range " + from.ToShortDateString() + " - " + to.ToShortDateString());
        }

    }
}