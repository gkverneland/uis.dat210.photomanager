﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Tests.Mocking
{
    public class FixSqlProblem
    {
        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            //This method is never been called, but I think the compiler will remove all "unnecessary" assemblies and 
            //without using the EntityFramework.SqlServer stuff the test fails.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
