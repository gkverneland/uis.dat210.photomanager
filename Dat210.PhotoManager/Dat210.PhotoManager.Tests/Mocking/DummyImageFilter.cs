﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Tests.Mocking
{
    public class ImageFilter : IImageFilter
    {
        public string Tag { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public ImageOrder Order { get; set; }

        public int? PageSize { get; set; }

        public int? PageNumber { get; set; }

        public int? MinimumRating { get; set; }
    }
}
