﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Ninject;

namespace Dat210.PhotoManager.Tests.Mocking
{
    public class TestData
    {
        private static readonly IApplicationConfiguration Config = Fake.Kernel.Get<IApplicationConfiguration>();
        private static readonly IFileRepository FileRepository = Fake.Kernel.Get<IFileRepository>();
        private static readonly IIndexingService IndexingService = Fake.Kernel.Get<IIndexingService>();

        private static bool _copied;

        public static void Copy()
        {
            if (_copied)
                return;
            Console.WriteLine("Copying test data");
            var libraryPath = Config.GetPhotoLibraryPath();
            var testDataPath = AppDomain.CurrentDomain.BaseDirectory;
            string[] files;
            try
            {
                files = Directory.GetFiles(testDataPath, "*.jpg", SearchOption.AllDirectories);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to list files in " + testDataPath, ex);
            }
            foreach (var image in files)
            {
                var fileName = Path.GetFileName(image);
                if (fileName == null) throw new Exception("Failed to get file name for image " + image);
                var newPath = Path.Combine(libraryPath, fileName);
                lock (FileRepository.EnsureFileIsAccessible(newPath, TimeSpan.FromMinutes(1)))
                {
                    if (File.Exists(newPath))
                    {
                        Console.WriteLine("Deleting " + newPath);
                        File.Delete(newPath);
                        Console.WriteLine("Deleted " + newPath);
                    }
                    Console.WriteLine("Copying " + newPath);
                    File.Copy(image, newPath);
                    Console.WriteLine("Copied " + newPath);
                }
            }
            Console.WriteLine("Indexing");
            IndexingService.Add();
            IndexingService.WaitUntilIndexed();

            var exceptions = IndexingService.GetExceptions();
            if (exceptions.Any())
            {
                Console.WriteLine("Exceptions from IndexingService: ");
                foreach (var exception in exceptions)
                {
                    Console.WriteLine(exception);
                    Console.WriteLine("--");
                    Console.WriteLine();
                }
                throw new Exception("IndexingService has failed");
            }

            Console.WriteLine("Running tests");

            _copied = true;
        }
    }
}
