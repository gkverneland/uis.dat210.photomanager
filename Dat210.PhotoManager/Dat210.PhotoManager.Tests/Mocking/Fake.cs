﻿using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Web;
using Ninject;

namespace Dat210.PhotoManager.Tests.Mocking
{
    public class Fake
    {
        private static readonly object Lock = new object();
        private static StandardKernel _kernel;
        public static StandardKernel Kernel
        {
            get
            {
                if (_kernel != null) return _kernel;
                lock (Lock)
                {
                    var kernel = new StandardKernel();
                    kernel.Load(typeof(MvcApplication).Assembly);
                    kernel.Load(typeof(IIndexingService).Assembly);
                    _kernel = kernel;
                    return _kernel;
                }
            }
        }
    }
}
