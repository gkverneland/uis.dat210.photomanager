﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Business.Fotofly;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Tests.Mocking;
using Dat210.PhotoManager.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace Dat210.PhotoManager.Tests
{
    [TestClass]
    public class MetadataTests
    {
        private readonly IImageLibrary _imageLibrary = Fake.Kernel.Get<IImageLibrary>();
        private readonly IMetadataProvider _metadataProvider = Fake.Kernel.Get<IMetadataProvider>();
        private readonly IApplicationConfiguration _config = Fake.Kernel.Get<IApplicationConfiguration>();

        [TestInitialize]
        public void Initialize()
        {
            TestData.Copy();
        }

        [TestMethod]
        public void WhenReadingMetadata_TagsShouldBeIncluded()
        {
            IMetadataWrapper metadata;
            var path = Path.Combine(_config.GetPhotoLibraryPath(), "wallpaper_08.jpg");
            try
            {
                
                metadata = _metadataProvider.GetMetadataWrapper(path);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to load metadata from file " + path);
            }

            Assert.IsTrue(metadata.Tags.Any(), "No tags read from image");
            Assert.IsTrue(metadata.Tags.Any(t => t == "wallpaper"), "Expected tag was not found");
        }

        [TestMethod]
        public void WhenReadingTagsFromLibrary_YouShouldGetAList()
        {
            var image = _imageLibrary.GetImageInfo(new ImageFilter {PageSize = 1}).Single();
            Assert.IsTrue(image.Tags != null, "Tag list was null");
        }

        [TestMethod]
        public void WhenReadingRating_YouShouldGetAValue()
        {
            bool hasImagesWithRating = _imageLibrary.GetImageInfo(new ImageFilter {MinimumRating = 1}).Any();
            Assert.IsTrue(hasImagesWithRating, "No images with rating 1 or above were found");
        }

        [TestMethod]
        public void WhenSettingRating_UpdateMetadata()
        {
            for (int testValue = 5; testValue >= 0; testValue--)
            {
                var image = _imageLibrary.GetImageInfo(new ImageFilter()).First(i => i.Rating != testValue);
                image.Rating = testValue;
                _imageLibrary.SaveImage(image);
                Console.WriteLine(image.OriginalFilePath);
                var metadata = _metadataProvider.GetMetadataWrapper(image.OriginalFilePath);
                Assert.IsTrue(metadata.Rating == testValue, "Rating was " + metadata.Rating + ", expected " + testValue);
            }
        }

        [TestMethod]
        public void WhenSettingTag_UpdateMetadata()
        {
            string[] tags = {"tag", "tfl", "tagsforlikes", "image", "me", "selfie"};

            var image = _imageLibrary.GetImageInfo(new ImageFilter {PageSize = 1}).Single();
            foreach (var tag in tags)
            {
                image.Tags.Add(tag);
                _imageLibrary.SaveImage(image);

                var metadata = _metadataProvider.GetMetadataWrapper(image.OriginalFilePath);
                Assert.IsTrue(metadata.Tags.Any(t => String.Equals(tag, t, StringComparison.InvariantCultureIgnoreCase)), 
                    "Tag was not saved to metadata (" + tag + " not found, found " +  string.Join(", ", metadata.Tags) + ")");
            }
            image.Tags.Clear();
            _imageLibrary.SaveImage(image);
        }

        [TestMethod]
        public void WhenRemovingTag_UpdateMetadata()
        {
            var imagesWithTags = _imageLibrary.GetImageInfo(new ImageFilter()).Where(i => i.Tags.Any()).ToArray();
            if (!imagesWithTags.Any())
                throw new Exception("No images with tags");
            Console.WriteLine("Removing tags from " + imagesWithTags.Count() + " images");
            foreach (var image in imagesWithTags)
            {
                Console.WriteLine(image.OriginalFilePath);
                var tagToRemove = image.Tags.First();
                Console.WriteLine("Removing tag " + tagToRemove);

                image.Tags.Remove(tagToRemove);
                _imageLibrary.SaveImage(image);

                var metadata = _metadataProvider.GetMetadataWrapper(image.OriginalFilePath);
                Assert.IsTrue(metadata.Tags.All(t => !String.Equals(tagToRemove, t, StringComparison.InvariantCultureIgnoreCase)),
                    "Tag was not removed: " + tagToRemove + " (image has tags '" + string.Join("', '", metadata.Tags) + "')");

                image.Tags.Add(tagToRemove);
                _imageLibrary.SaveImage(image);
                Console.WriteLine("Re-added tag " + tagToRemove);
            }
        }

        [TestMethod]
        public void WhenAddingComment_UpdateMetadata()
        {
            const string comment = "AddedTestComment";
            var imagesWithoutComments = _imageLibrary.GetImageInfo(new ImageFilter()).Where(i => String.IsNullOrEmpty(i.Comment)).ToArray();
            if (!imagesWithoutComments.Any())
                throw new Exception("No images without comment");
            Console.WriteLine("Adding comment to " + imagesWithoutComments.Count() + " images");
            foreach (var image in imagesWithoutComments)
            {
                
                Console.WriteLine(image.OriginalFilePath);
                string originalComment = image.Comment ?? "";
                image.Comment = comment;
                _imageLibrary.SaveImage(image);

                var metadata = _metadataProvider.GetMetadataWrapper(image.OriginalFilePath);
                Assert.IsTrue(metadata.Commment == comment, "Comment was not added to metadata");

                image.Comment = originalComment;
                _imageLibrary.SaveImage(image);
                Console.WriteLine("Re-added comment " + originalComment);
            }
        }
    }
}
