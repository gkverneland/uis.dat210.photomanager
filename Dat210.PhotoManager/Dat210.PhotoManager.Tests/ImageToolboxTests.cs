﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Tests.Mocking;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace Dat210.PhotoManager.Tests
{
    [TestClass]
    public class ImageToolboxTests
    {
        private readonly IMetadataProvider _metadata = Fake.Kernel.Get<IMetadataProvider>();
        private readonly IImageLibrary _imageLibrary = Fake.Kernel.Get<IImageLibrary>();
        private readonly IImageToolbox _toolbox = Fake.Kernel.Get<IImageToolbox>();
        private readonly IIndexingService _index = Fake.Kernel.Get<IIndexingService>();

        [TestMethod]
        public void TestResize()
        {
            const int maxWidth = 900;
            const int maxHeight = 600;
            var image = _imageLibrary.GetImageInfo(new ImageFilter() {PageSize = 1}).Single();
            Console.WriteLine("Resizing " + image.OriginalFilePath);

            var resized = _toolbox.ResizeImage(_imageLibrary.GetImage(image.Id).Data, maxHeight, maxWidth);

            var testFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".jpg");
            File.WriteAllBytes(testFile, resized);

            var metadata = _metadata.GetMetadataWrapper(testFile);
            Assert.IsTrue(metadata.Height <= maxHeight, "Max height exceeded (" + metadata.Height + ")");
            Assert.IsTrue(metadata.Width <= maxWidth, "Max width exceeded (" + metadata.Width + ")");
        }

        [TestMethod]
        public void TestRotate()
        {
            _index.WaitUntilIndexed();
            var image = _imageLibrary.GetImageInfo(new ImageFilter {PageSize = 1}).Single();
            Console.WriteLine("Rotating " + image.OriginalFilePath);
            var originalWidth = image.Width;
            var originalHeight = image.Height;
            _imageLibrary.RotateImage(image.Id, false);

            var metadata = _metadata.GetMetadataWrapper(image.OriginalFilePath);
            Assert.IsTrue(originalHeight == metadata.Width, 
                "Height before rotating (" + originalHeight + ") should be the new width (" + metadata.Width + ")");
            Assert.IsTrue(originalWidth == metadata.Height,
                "Width before rotating (" + originalWidth + ") should be the new height (" + metadata.Height + ")");

            //rotate back
            _imageLibrary.RotateImage(image.Id, true);
        }
    }
}
