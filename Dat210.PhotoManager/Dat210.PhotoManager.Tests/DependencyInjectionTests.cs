﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Business.Fotofly;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Data.Indexing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace Dat210.PhotoManager.Tests
{
    [TestClass]
    public class DependencyInjectionTests
    {
        [TestMethod]
        public void DependencyInjection_WhenLoaded_ResolveIndexRepository()
        {
            var kernel = new StandardKernel();
            kernel.Load(typeof(IIndexRepository).Assembly);
            var indexRepository  = kernel.Get<IIndexRepository>();
            Assert.IsFalse(indexRepository == null);

            Console.WriteLine("Resolved implementation of IIndexRepository: " + indexRepository.GetType());
        }
        
        [TestMethod]
        public void IndexingService_WhenGivenNoFileRepository_ThrowArgumentNullException()
        {
            ArgumentNullException exception = null;
            try
            {
                using (new IndexingService(null, null, null, null))
                {
                    
                }
            }
            catch (ArgumentNullException ex)
            {
                exception = ex;
            }

            Assert.IsFalse(exception == null);

        }

        [TestMethod]
        public void IndexingService_WhenResolved_NoExceptionIsThrown()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(typeof(IIndexingService).Assembly);

                using (kernel.Get<IIndexingService>())
                {
                    
                }
            }
        }
    }
}
