﻿using System;
using System.IO;
using System.Linq;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Tests.Mocking;
using Dat210.PhotoManager.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace Dat210.PhotoManager.Tests
{
    [TestClass]
    public class FileRepositoryTests
    {
        private readonly IFileRepository _fileRepository = Fake.Kernel.Get<IFileRepository>();

        [TestMethod]
        [DeploymentItem("TestData")]
        public void ListImages_WhenImagesInDirectory_ReturnImages()
        {
            var paths = _fileRepository.ListImageFiles(AppDomain.CurrentDomain.BaseDirectory, true);
            Assert.IsTrue(paths.Any());
        }

        [TestMethod]
        public void ListImages_WhenNoImagesInDirectory_ReturnNoImages()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "EmptyFolder");
            if (Directory.Exists(path))
            {
                foreach (var file in Directory.GetFiles(path, "*.*")) 
                {
                    File.Delete(file);
                }
            }
            else
            {
                Directory.CreateDirectory(path);
            }
            var paths = _fileRepository.ListImageFiles(path, true);
            Assert.IsFalse(paths.Any());
        }
    }
}
