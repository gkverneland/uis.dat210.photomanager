﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dat210.PhotoManager.Business;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Data.Indexing;
using Dat210.PhotoManager.Tests.Mocking;
using Dat210.PhotoManager.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace Dat210.PhotoManager.Tests
{
    [TestClass]
    public class NavigationTests
    {
        private readonly IImageLibrary _imageLibrary = Fake.Kernel.Get<IImageLibrary>();
        private readonly IIndexingService _indexingService = Fake.Kernel.Get<IIndexingService>();

        private void WaitForIndexing()
        {
            DateTime start = DateTime.Now;
            while (_indexingService.GetLastRun() == DateTime.MinValue)
            {
                TimeSpan waited = DateTime.Now - start;
                if (waited > TimeSpan.FromSeconds(30))
                {
                    #if !DEBUG
                        throw new Exception("Waited for " + waited.TotalSeconds + " seconds for images to be indexed");
                    #endif
                }
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Som en bruker vil jeg kunne navigere i bilder
        ///  - Når brukeren trykker på knappen "neste" vises neste bilde i arkivet
        ///  - Når brukeren trykker på knappen "forrige" vises forrige bilde i arkivet
        /// </summary>
        [TestMethod]
        public void Navigation_WhenViewingImage_ViewNext()
        {
            WaitForIndexing();
            var filter = new ImageFilter();
            var images = _imageLibrary.GetImageInfo(filter);
            Assert.IsTrue(images.Any(), "No images in library");
            var firstImage = images.First();

            var secondImage = _imageLibrary.GetNextImageId(firstImage.Id, filter);
            Assert.IsTrue(secondImage.HasValue, "The first image should have a next image");

            var beforeSecondImage = _imageLibrary.GetPreviousImageId(secondImage.Value, filter);
            Assert.IsTrue(beforeSecondImage.HasValue, "The second image should have a previous image");
            Assert.IsTrue(beforeSecondImage.Value == firstImage.Id, "The second image should have the first image as previous image");
        }

    }
}
