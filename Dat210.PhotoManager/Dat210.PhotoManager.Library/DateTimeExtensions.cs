﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Library
{
    public static class DateTimeExtensions
    {
        public static bool TimeEquals(this DateTime d1, DateTime d2)
        {
            return d1.Year == d2.Year
                   && d1.Month == d2.Month
                   && d1.Day == d2.Day
                   && d1.Hour == d2.Hour
                   && d1.Minute == d2.Minute
                   && d1.Second == d2.Second;
        }
    }
}
