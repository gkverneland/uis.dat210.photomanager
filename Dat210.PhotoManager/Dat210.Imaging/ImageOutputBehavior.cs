using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Sherwood.Imaging
{
    public abstract class ImageOutputBehavior
    {
        public void SaveToStream(Image image, Stream stream)
        {
            image.Save(stream, GetEncoderInfo(), GetEncoderParameters());
        }

        protected abstract EncoderParameters GetEncoderParameters();

        protected abstract ImageCodecInfo GetEncoderInfo();

        protected static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        public abstract string ContentType { get; }
    }
}