using System;
using System.Drawing;

namespace Sherwood.Imaging
{
    public interface IFilter
    {
        /// <exception cref="ArgumentNullException"><c>image</c> is null.</exception>
        Image ApplyFilter(Image image);
    }
}