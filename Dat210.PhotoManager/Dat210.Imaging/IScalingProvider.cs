using System.Drawing;

namespace Sherwood.Imaging
{
    public interface IScalingProvider
    {
        ScalingParameters GetScaling(Image sourceImage, Image destinationImage);
    }
}