using System;
using System.Collections.Generic;

namespace Sherwood.Imaging
{
    public class TemplateContainer : ITemplateContainer
    {
        private readonly List<Template> templates = new List<Template>();

        public void RegisterTemplate(Template template)
        {
            templates.Add(template);
        }

        /// <exception cref="ArgumentOutOfRangeException"><c>name</c> is out of range.</exception>
        public Template Resolve(string name)
        {
            foreach (var template in templates)
            {
                if (string.Equals(template.Name, name, StringComparison.InvariantCultureIgnoreCase))
                    return template;
            }
            throw new ArgumentOutOfRangeException("name", string.Format("The template named '{0}' was not found", name));
        }
    }
}