using System;
using System.Drawing;

namespace Sherwood.Imaging
{
    [Serializable]
    public class ScalingParameters
    {
        public ScalingParameters(RectangleF sourceRectangle, RectangleF destinationRectangle)
        {
            SourceRectangle = sourceRectangle;
            DestinationRectangle = destinationRectangle;
        }

        public RectangleF SourceRectangle{ get; private set;}
        public RectangleF DestinationRectangle { get; private set; }
    }
}