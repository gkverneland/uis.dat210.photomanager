using System.Drawing;

namespace Sherwood.Imaging
{
    public interface IImageProvider
    {
        Image GetImage();
    }
}