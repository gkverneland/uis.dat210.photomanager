using System.Drawing;

namespace Sherwood.Imaging
{
    public interface IRectangleProvider
    {
        RectangleF GetRectangle(Image image);
    }
}