using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class AdjustRectangleProvider : IRectangleProvider
    {
        public float? Top { get; set; }
        public float? Right { get; set; }
        public float? Bottom { get; set; }
        public float? Left { get; set; }
        public IRectangleProvider Original { get; set; }

        public AdjustRectangleProvider(IRectangleProvider original, float? top, float? right, float? bottom, float? left)
        {
            Top = top;
            Right = right;
            Bottom = bottom;
            Left = left;
            Original = original;
        }

        public RectangleF GetRectangle(Image image)
        {
            var org = Original.GetRectangle(image);
            var rectangle = new RectangleF(org.X - (Left ?? 0f), org.Y - (Top ?? 0f), org.Width + (Left ?? 0f) + (Right ?? 0f), org.Height + (Top ?? 0f) + (Bottom ?? 0f));
            return rectangle;
        }
    }
}