using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class TemplateImageProvider : IImageProvider
    {
        private readonly ITemplateContainer container;

        public TemplateImageProvider(ITemplateContainer container, IStringProvider templateName)
        {
            this.container = container;
            TemplateName = templateName;
        }

        IStringProvider TemplateName { get; set; }

        public Image GetImage()
        {
            return container.Resolve(TemplateName.GetString()).GetImage();
        }
    }
}