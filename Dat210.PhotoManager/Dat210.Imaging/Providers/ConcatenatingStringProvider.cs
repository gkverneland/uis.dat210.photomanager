﻿using System.Linq;

namespace Sherwood.Imaging.Providers
{
    public class ConcatenatingStringProvider : IStringProvider
    {
        private readonly IStringProvider[] _stringProviders;

        public ConcatenatingStringProvider(params IStringProvider[] stringProviders)
        {
            _stringProviders = stringProviders;
        }

        public string GetString()
        {
            return _stringProviders.Aggregate(string.Empty, (s, provider) => s + provider.GetString());
        }
    }
}