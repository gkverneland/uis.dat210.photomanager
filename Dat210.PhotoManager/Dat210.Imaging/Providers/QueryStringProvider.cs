using System.Web;

namespace Sherwood.Imaging.Providers
{
    public class QueryStringProvider : IStringProvider
    {
        public string Key { get; set; }

        public QueryStringProvider(string key)
        {
            Key = key;
        }

        public string GetString()
        {
            return HttpContext.Current.Request.QueryString[Key];
        }
    }
}