using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class StaticRectangleProvider :IRectangleProvider
    {
        public RectangleF Rectangle { get; set; }

        public StaticRectangleProvider(RectangleF rectangle)
        {
            Rectangle = rectangle;
        }

        public RectangleF GetRectangle(Image image)
        {
            return Rectangle;
        }
    }
}