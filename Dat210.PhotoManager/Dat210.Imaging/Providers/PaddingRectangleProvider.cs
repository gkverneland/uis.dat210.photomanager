using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class PaddingRectangleProvider : IRectangleProvider
    {
        public float? Top { get; set; }
        public float? Right { get; set; }
        public float? Bottom { get; set; }
        public float? Left { get; set; }
        public ISizeProvider SizeProvider { get; set; }

        public PaddingRectangleProvider(float? top, float? right, float? bottom, float? left, ISizeProvider sizeProvider)
        {
            Top = top;
            Right = right;
            Bottom = bottom;
            Left = left;
            SizeProvider = sizeProvider;
        }

        public RectangleF GetRectangle(Image image)
        {
            var size = SizeProvider.GetSize(image);
            var location = new PointF(Left ?? (image.Width - (Right ?? (image.Width - size.Width)/2) - size.Width),
                                      Top ?? (image.Height - (Bottom ?? (image.Height - size.Height)/2)) - size.Height);
            return new RectangleF(location, size);
        }
    }
}