namespace Sherwood.Imaging.Providers
{
    public class StaticBooleanProvider : IBooleanProvider
    {
        public bool Value { get; set; }

        public StaticBooleanProvider(bool value)
        {
            Value = value;
        }

        public bool GetValue()
        {
            return Value;
        }
    }
}