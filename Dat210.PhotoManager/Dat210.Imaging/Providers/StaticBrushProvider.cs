using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class StaticBrushProvider : IBrushProvider
    {
        public Brush Brush { get; set; }

        public StaticBrushProvider(Brush brush)
        {
            Brush = brush;
        }

        public Brush GetBrush()
        {
            return Brush;
        }
    }
}