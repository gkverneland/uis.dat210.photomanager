using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class StaticSizeProvider : ISizeProvider
    {
        public INumberProvider WidthProvider { get; set; }
        public INumberProvider HeightProvider { get; set; }

        public StaticSizeProvider(INumberProvider widthProvider, INumberProvider heightProvider)
        {
            WidthProvider = widthProvider;
            HeightProvider = heightProvider;
        }

        public SizeF GetSize(Image image)
        {
            return new SizeF(WidthProvider.GetNumber(), HeightProvider.GetNumber());
        }
    }
}