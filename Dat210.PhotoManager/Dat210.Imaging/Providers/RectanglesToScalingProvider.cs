using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class RectanglesToScalingProvider : IScalingProvider
    {
        public IRectangleProvider SourceRectangleProvider { get; set; }
        public IRectangleProvider DestinationRectangleProvider { get; set; }

        public RectanglesToScalingProvider(IRectangleProvider destinationRectangleProvider)
            : this(new CopyImageRectangleProvider(), destinationRectangleProvider)
        {
        }

        public RectanglesToScalingProvider(IRectangleProvider sourceRectangleProvider, IRectangleProvider destinationRectangleProvider)
        {
            SourceRectangleProvider = sourceRectangleProvider;
            DestinationRectangleProvider = destinationRectangleProvider;
        }

        public ScalingParameters GetScaling(Image sourceImage, Image destinationImage)
        {
            return new ScalingParameters(SourceRectangleProvider.GetRectangle(sourceImage),
                               DestinationRectangleProvider.GetRectangle(destinationImage));
        }
    }
}