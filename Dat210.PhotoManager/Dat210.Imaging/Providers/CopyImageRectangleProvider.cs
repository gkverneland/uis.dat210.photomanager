using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class CopyImageRectangleProvider :IRectangleProvider
    {
        public IImageProvider Image { get; private set; }
        public PointF Location{ get; private set;}

        public CopyImageRectangleProvider() : this(PointF.Empty)
        {
        }

        public CopyImageRectangleProvider(PointF location) : this(null, location)
        {
        }

        public CopyImageRectangleProvider(IImageProvider image, PointF location)
        {
            Image = image;
            Location = location;
        }


        public RectangleF GetRectangle(Image image)
        {
            return new RectangleF(Location, ((Image == null ? null : Image.GetImage()) ?? image).Size);
        }
    }
}