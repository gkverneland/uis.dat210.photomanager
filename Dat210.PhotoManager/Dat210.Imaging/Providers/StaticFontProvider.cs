using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class StaticFontProvider : IFontProvider
    {
        public Font Font { get; set; }

        public StaticFontProvider(Font font)
        {
            Font = font;
        }

        public Font GetFont()
        {
            return Font;
        }
    }
}