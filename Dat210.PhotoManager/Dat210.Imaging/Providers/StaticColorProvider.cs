using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class StaticColorProvider : IColorProvider
    {
        public Color Color { get; set; }

        public StaticColorProvider(Color color)
        {
            Color = color;
        }

        public Color GetColor()
        {
            return Color;
        }
    }
}