using System.Drawing;
using System.Drawing.Imaging;

namespace Sherwood.Imaging.Providers
{
    public class NewImageProvider : IImageProvider
    {
        public INumberProvider Width { get; set; }
        public INumberProvider Height { get; set; }
        public PixelFormat PixelFormat { get; set; }

        public NewImageProvider(INumberProvider width, INumberProvider height, PixelFormat pixelFormat)
        {
            Width = width;
            Height = height;
            PixelFormat = pixelFormat;
        }

        public Image GetImage()
        {
            return new Bitmap((int)Width.GetNumber(), (int)Height.GetNumber(), PixelFormat);
        }
    }
}