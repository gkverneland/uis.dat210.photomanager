using System;

namespace Sherwood.Imaging.Providers
{
    public class StringToBooleanProvider : IBooleanProvider
    {
        public IStringProvider StringProvider { get; set; }

        public StringToBooleanProvider(IStringProvider stringProvider)
        {
            StringProvider = stringProvider;
        }

        public bool GetValue()
        {
            return string.Equals(StringProvider.GetString() , "true", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}