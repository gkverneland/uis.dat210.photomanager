namespace Sherwood.Imaging.Providers
{
    public class StaticStringProvider : IStringProvider
    {
        public StaticStringProvider(string value)
        {
            Value = value;
        }

        public string GetString()
        {
            return Value;
        }

        private string Value { get; set; }
    }
}