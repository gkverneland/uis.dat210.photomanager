using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class StaticImageProvider : IImageProvider
    {
        public Image Image { get; set; }

        public StaticImageProvider(Image image)
        {
            Image = image;
        }

        public Image GetImage()
        {
            return Image;
        }
    }
}