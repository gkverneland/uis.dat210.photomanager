using System.Drawing;

namespace Sherwood.Imaging.Providers
{
    public class DynamicFontProvider : IFontProvider
    {
        public IStringProvider FontFamily { get; set; }
        public INumberProvider FontSize { get; set; }
        public IBooleanProvider Bold { get; set; }
        public IBooleanProvider Italic { get; set; }
        public IBooleanProvider Underline { get; set; }

        public DynamicFontProvider(IStringProvider fontFamily, INumberProvider fontSize, IBooleanProvider bold, IBooleanProvider italic, IBooleanProvider underline)
        {
            FontFamily = fontFamily;
            FontSize = fontSize;
            Bold = bold;
            Italic = italic;
            Underline = underline;
        }

        public Font GetFont()
        {
            FontStyle style = FontStyle.Regular;
            if (Bold.GetValue()) style |= FontStyle.Bold;
            if (Italic.GetValue()) style |= FontStyle.Italic;
            if (Underline.GetValue()) style |= FontStyle.Underline;
            return new Font(FontFamily.GetString(),
                            FontSize.GetNumber(),
                            style);
        }
    }
}