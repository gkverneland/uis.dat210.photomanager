namespace Sherwood.Imaging.Providers
{
    public class StaticNumberProvider : INumberProvider
    {
        public StaticNumberProvider(float value)
        {
            Value = value;
        }

        public float GetNumber()
        {
            return Value;
        }

        private float Value { get; set; }
    }
}