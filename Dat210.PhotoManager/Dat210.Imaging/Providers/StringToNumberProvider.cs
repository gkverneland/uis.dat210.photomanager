using System;

namespace Sherwood.Imaging.Providers
{
    public class StringToNumberProvider : INumberProvider
    {
        public IStringProvider StringProvider { get; set; }

        public StringToNumberProvider(IStringProvider stringProvider)
        {
            StringProvider = stringProvider;
        }

        public float GetNumber()
        {
            return Convert.ToSingle(StringProvider.GetString());
        }
    }
}