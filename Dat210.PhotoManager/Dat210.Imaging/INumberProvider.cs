namespace Sherwood.Imaging
{
    public interface INumberProvider
    {
        float GetNumber();
    }
}