using System.Drawing;

namespace Sherwood.Imaging
{
    public interface IBrushProvider
    {
        Brush GetBrush();
    }
}