using System.Drawing.Imaging;

namespace Sherwood.Imaging.Behaviors
{
    public class PngImageOutputBehavior : ImageOutputBehavior
    {
        public override string ContentType
        {
            get { return "image/png"; }
        }

        protected override EncoderParameters GetEncoderParameters()
        {
            return new EncoderParameters(0);
        }

        protected override ImageCodecInfo GetEncoderInfo()
        {
            return GetEncoderInfo("image/png");
        }
    }
}