using System.Drawing.Imaging;

namespace Sherwood.Imaging.Behaviors
{
    public class JpegImageOutputBehavior : ImageOutputBehavior
    {
        public INumberProvider JpegQuality { get; set; }

        public JpegImageOutputBehavior(INumberProvider jpegQuality)
        {
            JpegQuality = jpegQuality;
        }

        public override string ContentType
        {
            get { return "image/jpeg"; }
        }

        protected override EncoderParameters GetEncoderParameters()
        {
            var eps = new EncoderParameters(1);
            eps.Param[0] = new EncoderParameter(Encoder.Quality, (long) JpegQuality.GetNumber());
            return eps;
        }

        protected override ImageCodecInfo GetEncoderInfo()
        {
            return GetEncoderInfo("image/jpeg");
        }
    }
}