using System;
using System.Drawing;

namespace Sherwood.Imaging.Behaviors
{
    public class FixedWidthResizeBehavior : ResizeBehavior
    {
        public override void DoResize(Size original, int? maxWidth, int? maxHeight, out Size newSize, out Rectangle sourceRectangle)
        {
            sourceRectangle = new Rectangle(Point.Empty, original);

            double width = original.Width;
            double height = original.Height;
            double ratio = width/height;
            newSize = maxWidth.HasValue ? new Size(maxWidth.Value, Convert.ToInt32(maxWidth.Value / ratio)) : original;
        }
    }
}