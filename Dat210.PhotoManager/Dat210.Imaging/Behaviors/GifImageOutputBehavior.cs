using System.Drawing.Imaging;

namespace Sherwood.Imaging.Behaviors
{
    public class GifImageOutputBehavior : ImageOutputBehavior
    {
        public override string ContentType
        {
            get { return "image/gif"; }
        }

        protected override EncoderParameters GetEncoderParameters()
        {
            return new EncoderParameters(0);
        }

        protected override ImageCodecInfo GetEncoderInfo()
        {
            return GetEncoderInfo("image/gif");
        }
    }
}