using System.Drawing;

namespace Sherwood.Imaging.Behaviors
{
    public class SimpleResizeBehavior : ResizeBehavior
    {
        public override void DoResize(Size original, int? maxWidth, int? maxHeight, out Size newSize, out Rectangle sourceRectangle)
        {
            sourceRectangle = new Rectangle(Point.Empty, original);
            double width = original.Width;
            double height = original.Height;
            double ratio = width/height;

            if (maxWidth.HasValue && width > maxWidth.Value)
            {
                width = maxWidth.Value;
                height = width/ratio;
            }
            if (maxHeight.HasValue && height > maxHeight.Value)
            {
                height = maxHeight.Value;
                width = height*ratio;
            }
            newSize = new Size((int) width, (int) height);
        }
    }
}