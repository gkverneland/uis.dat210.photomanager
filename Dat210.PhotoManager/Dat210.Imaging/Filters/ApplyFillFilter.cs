using System;
using System.Drawing;

namespace Sherwood.Imaging.Filters
{
    public class ApplyFillFilter : IFilter
    {
        public IBrushProvider FillBrush { get; set; }

        public ApplyFillFilter(IBrushProvider fillBrush)
        {
            FillBrush = fillBrush;
        }

        /// <exception cref="ArgumentNullException"><c>original</c> is null.</exception>
        public Image ApplyFilter(Image original)
        {
            if (original == null) throw new ArgumentNullException("original");

            using (var graphics = Graphics.FromImage(original))
            {
                graphics.FillRectangle(FillBrush.GetBrush(), 0, 0, original.Width, original.Height);
                return original;
            }
        }
    }
}