using System.Drawing;
using System.Drawing.Drawing2D;

namespace Sherwood.Imaging.Filters
{
    public class DrawImageFilter : IFilter
    {
        public IImageProvider ImageProvider { get; set; }
        public IScalingProvider ScalingProvider { get; set; }

        public DrawImageFilter(IImageProvider imageProvider, IScalingProvider scalingProvider)
        {
            ImageProvider = imageProvider;
            ScalingProvider = scalingProvider;
        }

        public Image ApplyFilter(Image image)
        {
            using (var graphics = Graphics.FromImage(image))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                var drawImage = ImageProvider.GetImage();
                var scaling = ScalingProvider.GetScaling(drawImage, image);
                graphics.DrawImage(drawImage,
                                   scaling.DestinationRectangle,
                                   scaling.SourceRectangle, GraphicsUnit.Pixel);
                return image;
            }
        }
    }
}