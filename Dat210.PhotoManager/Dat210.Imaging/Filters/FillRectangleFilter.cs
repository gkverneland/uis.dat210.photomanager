using System.Drawing;

namespace Sherwood.Imaging.Filters
{
    public class FillRectangleFilter : IFilter
    {
        public IRectangleProvider RectangleProvider { get; set; }
        public IBrushProvider BrushProvider { get; set; }

        public FillRectangleFilter(IRectangleProvider rectangleProvider, IBrushProvider brushProvider)
        {
            RectangleProvider = rectangleProvider;
            BrushProvider = brushProvider;
        }

        public Image ApplyFilter(Image image)
        {
            using (var graphics = Graphics.FromImage(image))
            {
                graphics.FillRectangle(BrushProvider.GetBrush(), RectangleProvider.GetRectangle(image));
                return image;
            }
        }
    }
}