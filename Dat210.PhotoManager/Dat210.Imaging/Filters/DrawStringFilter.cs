using System.Drawing;

namespace Sherwood.Imaging.Filters
{
    public class DrawStringFilter : IFilter
    {
        public IBrushProvider BrushProvider { get; set; }
        public IStringProvider TextProvider { get; set; }
        public IFontProvider FontProvider { get; set; }
        public IRectangleProvider LayoutRectangleProvider { get; set; }

        public DrawStringFilter(IBrushProvider brushProvider, IStringProvider textProvider, IFontProvider fontProvider, IRectangleProvider layoutRectangleProvider)
        {
            BrushProvider = brushProvider;
            TextProvider = textProvider;
            FontProvider = fontProvider;
            LayoutRectangleProvider = layoutRectangleProvider;
        }

        public Image ApplyFilter(Image image)
        {
            using (var graphics = Graphics.FromImage(image))
            {
                var text = TextProvider.GetString();
                graphics.DrawString(text,
                                    FontProvider.GetFont(),
                                    BrushProvider.GetBrush(),
                                    LayoutRectangleProvider.GetRectangle(image));

                return image;
            }
        }
    }
}