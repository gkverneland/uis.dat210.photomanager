using System.Drawing;
using System.Drawing.Imaging;

namespace Sherwood.Imaging.Filters
{
    public class ChangePixelFormatFilter : IFilter
    {
        public PixelFormat PixelFormat { get; set; }

        public ChangePixelFormatFilter(PixelFormat pixelFormat)
        {
            PixelFormat = pixelFormat;
        }

        public Image ApplyFilter(Image original)
        {
            var bitmap = new Bitmap(original.Width, original.Height, PixelFormat);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawImageUnscaledAndClipped(original, new Rectangle(0, 0, original.Width, original.Height));
                return bitmap;
            }
        }
    }
}