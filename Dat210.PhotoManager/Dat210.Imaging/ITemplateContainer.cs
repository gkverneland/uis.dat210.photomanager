using System;

namespace Sherwood.Imaging
{
    public interface ITemplateContainer
    {
        /// <exception cref="ArgumentOutOfRangeException"><c>name</c> is out of range.</exception>
        Template Resolve(string name);
    }
}