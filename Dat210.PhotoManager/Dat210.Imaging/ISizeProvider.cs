using System.Drawing;

namespace Sherwood.Imaging
{
    public interface ISizeProvider
    {
        SizeF GetSize(Image image);
    }
}