using System.Drawing;

namespace Sherwood.Imaging
{
    public abstract class ResizeBehavior
    {
        public abstract void DoResize(Size original, int? maxWidth, int? maxHeight, out Size newSize, out Rectangle sourceRectangle);
    }
}