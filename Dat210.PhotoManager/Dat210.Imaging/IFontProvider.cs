using System.Drawing;

namespace Sherwood.Imaging
{
    public interface IFontProvider
    {
        Font GetFont();
    }
}