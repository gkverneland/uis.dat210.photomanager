namespace Sherwood.Imaging
{
    public interface IBooleanProvider
    {
        bool GetValue();
    }
}