namespace Sherwood.Imaging
{
    public interface IStringProvider
    {
        string GetString();
    }
}