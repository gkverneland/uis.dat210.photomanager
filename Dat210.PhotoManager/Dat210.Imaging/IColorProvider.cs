using System.Drawing;

namespace Sherwood.Imaging
{
    public interface IColorProvider
    {
        Color GetColor();
    }
}