﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Contracts
{
    public interface IFile
    {
        byte[] Data { get; }
        string ContentType { get; }
    }
}
