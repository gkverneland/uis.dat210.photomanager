﻿using System;
using System.Collections.Generic;

namespace Dat210.PhotoManager.Contracts
{
    public interface IImageLibrary
    {
        IEnumerable<IImage> GetImageInfo(IImageFilter filter);

        IImage GetImageInfo(Guid id);

        IFile GetSmallThumbnail(Guid id);

        IFile GetPreview(Guid id);

        IFile GetImage(Guid id);

        IEnumerable<string> GetMostlyUsedTags(int count);
            
        Guid? GetNextImageId(Guid current, IImageFilter filter);
        Guid? GetPreviousImageId(Guid current, IImageFilter filter);

        string CreatePreview(byte[] image, string extension);

        string CreateSmallThumbnail(byte[] image, string extension);

        void AddImage(string path);

        void UpdateImage(string path);

        void SaveImage(IImage image);

        void RotateImage(Guid id, bool counterClockwise);

        void DeleteImage(Guid id);
    }
}
