﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Contracts
{
    [Flags]
    public enum ImageOrder
    {
        None =         0x0,
        Descending =   0x1,
        Ascending =    0x10,
        DateTaken =    0x100,
        Pixels =       0x1000,
        Name =         0x100000
    }
}
