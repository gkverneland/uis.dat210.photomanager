﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Contracts
{
    public interface IMetadataProvider
    {
        IMetadataWrapper GetMetadataWrapper(string path);
    }
}
