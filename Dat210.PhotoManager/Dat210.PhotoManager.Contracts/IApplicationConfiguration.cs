﻿namespace Dat210.PhotoManager.Contracts
{
    public interface IApplicationConfiguration
    {
        string GetPhotoLibraryPath();
        string GetThumbnailPath();

        void SetPhotoLibraryPath(string value);
        void SetThumbnailPath(string value);
    }
}
