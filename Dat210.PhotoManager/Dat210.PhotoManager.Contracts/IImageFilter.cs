﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Contracts
{
    public interface IImageFilter
    {
        string Tag { get; }

        DateTime? FromDate { get; }
        DateTime? ToDate { get; }

        ImageOrder Order { get; }

        int? PageSize { get; }

        int? PageNumber { get; }

        int? MinimumRating { get; }
    }
}
