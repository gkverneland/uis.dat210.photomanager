﻿using System;

namespace Dat210.PhotoManager.Contracts
{
    public interface IFileInformation
    {
        string Path { get; }
        string Name { get; }
        string Extension { get; }
        DateTime Modified { get; }
        DateTime Created { get; }
    }
}