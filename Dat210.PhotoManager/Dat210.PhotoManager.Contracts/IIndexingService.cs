﻿using System;
using System.Collections.Generic;

namespace Dat210.PhotoManager.Contracts
{
    public interface IIndexingService : IDisposable
    {
        /// <summary>
        /// Returns a string indicating what the indexing service is doing
        /// </summary>
        string GetStatus();

        IEnumerable<Exception> GetExceptions();

        /// <summary>
        /// Start adding images without waiting for it to complete
        /// </summary>
        void Add();

        /// <summary>
        /// Start removing images without waiting for it to complete
        /// </summary>
        void Remove();

        /// <summary>
        /// Returns the time adding to index was completed
        /// </summary>
        DateTime GetLastRun();

        void WaitUntilIndexed();

        DateTime GetTimeStarted();
    }
}
