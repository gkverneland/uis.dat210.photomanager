﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Contracts
{
    /// <summary>
    /// Wrapper that allows you to read and write metadata from image
    /// Saves on dispose or Save()
    /// </summary>
    public interface IMetadataWrapper : IDisposable
    {
        IEnumerable<string> Tags { get; set; }
        DateTime Taken { get; set; }
        string CameraModel { get; }
        string Commment { get; set; }
        int Rating { get; set; }
        int Height { get; }
        int Width { get; }
        void Save();
    }
}
