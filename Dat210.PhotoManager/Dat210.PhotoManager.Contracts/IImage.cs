﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Contracts
{
    public interface IImage
    {
        Guid Id { get; }
        string Name { get; }
        string OriginalFilePath { get; }
        string Hash { get; set; }
        string SmallThumbnailFilePath { get; set; }
        string PreviewFilePath { get; set; }
        DateTime Created { get; }
        DateTime Modified { get; set; }
        DateTime Taken { get; set; }
        int Rating { get; set; }
        int Width { get; set; }
        int Height { get; set; }
        string CameraModel { get; set; }
        string Comment { get; set; }
        ICollection<string> Tags { get; }
    }
}
