﻿namespace Dat210.PhotoManager.Contracts
{
    public interface IImageToolbox
    {
        byte[] ResizeImage(byte[] image, int maxWidth, int maxHeight);

        /// <summary>
        /// Rotates an image 90 degrees
        /// </summary>
        /// <param name="image">Image to rotate</param>
        /// <param name="counterClockwise">True to rotate counter clockwise, False to rotate clockwise</param>
        /// <returns></returns>
        byte[] RotateImage(byte[] image, bool counterClockwise);
    }
}
