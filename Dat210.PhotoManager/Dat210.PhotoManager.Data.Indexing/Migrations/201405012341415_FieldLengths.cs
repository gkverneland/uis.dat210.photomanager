namespace Dat210.PhotoManager.Data.Indexing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldLengths : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Images", "Name", c => c.String(maxLength: 255));
            AlterColumn("dbo.Images", "OriginalFilePath", c => c.String(maxLength: 1024));
            AlterColumn("dbo.Images", "Hash", c => c.String(maxLength: 1024));
            AlterColumn("dbo.Images", "SmallThumbnailFilePath", c => c.String(maxLength: 1024));
            AlterColumn("dbo.Images", "PreviewFilePath", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Images", "PreviewFilePath", c => c.String());
            AlterColumn("dbo.Images", "SmallThumbnailFilePath", c => c.String());
            AlterColumn("dbo.Images", "Hash", c => c.String());
            AlterColumn("dbo.Images", "OriginalFilePath", c => c.String());
            AlterColumn("dbo.Images", "Name", c => c.String());
        }
    }
}
