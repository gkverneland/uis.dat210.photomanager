using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Dat210.PhotoManager.Data.Indexing.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Sql.SqlIndexDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}
