namespace Dat210.PhotoManager.Data.Indexing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.ImageTags",
                c => new
                    {
                        ImageId = c.Guid(nullable: false),
                        TagName = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.ImageId, t.TagName })
                .ForeignKey("dbo.Images", t => t.ImageId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagName, cascadeDelete: true)
                .Index(t => t.ImageId)
                .Index(t => t.TagName);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ImageTags", "TagName", "dbo.Tags");
            DropForeignKey("dbo.ImageTags", "ImageId", "dbo.Images");
            DropIndex("dbo.ImageTags", new[] { "TagName" });
            DropIndex("dbo.ImageTags", new[] { "ImageId" });
            DropTable("dbo.ImageTags");
            DropTable("dbo.Tags");
        }
    }
}
