namespace Dat210.PhotoManager.Data.Indexing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Indexes : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Images", "OriginalFilePath", true, "IDX_OriginalFilePath");
            CreateIndex("dbo.Images", "Hash", false, "IDX_Hash");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Images", "IDX_OriginalFilePath");
            DropIndex("dbo.Images", "IDX_Hash");
        }
    }
}
