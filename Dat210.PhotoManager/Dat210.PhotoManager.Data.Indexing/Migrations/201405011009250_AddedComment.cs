namespace Dat210.PhotoManager.Data.Indexing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Images", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Images", "Comment");
        }
    }
}
