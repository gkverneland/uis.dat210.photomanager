// <auto-generated />
namespace Dat210.PhotoManager.Data.Indexing.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.2-21211")]
    public sealed partial class AddedDimensions : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedDimensions));
        
        string IMigrationMetadata.Id
        {
            get { return "201402122051273_AddedDimensions"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
