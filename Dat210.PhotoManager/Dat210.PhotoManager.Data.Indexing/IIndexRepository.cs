﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Data.Indexing
{
    public interface IIndexRepository
    {
        void Add(IImage image);
        void Save(IImage image);
        void Remove(Guid id);
        IEnumerable<IImage> GetImages(IImageFilter filter);
        IEnumerable<string> GetAllImagePaths();

        IEnumerable<string> GetMostlyUsedTags(int count);
            
        /// <summary>
        /// Look-up a single image based on id
        /// Throws and exception if it does not exist in index
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IImage GetImage(Guid id);

        Guid? GetNextImageId(Guid current, IImageFilter filter);

        Guid? GetPreviousImageId(Guid current, IImageFilter filter);

        /// <summary>
        /// Look-up a single image based on original file path
        /// Returns null if it does not exist in index
        /// </summary>
        IImage GetImage(string originalFilePath);

        bool Contains(string originalFilePath, DateTime lastModified);
        bool Contains(string originalFilePath);
    }
}
