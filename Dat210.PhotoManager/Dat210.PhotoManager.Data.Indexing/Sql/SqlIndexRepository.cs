﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.Indexing.Exceptions;
using Dat210.PhotoManager.Data.Indexing.Sql.Entities;
using Dat210.PhotoManager.Library;
using log4net;
using IsolationLevel = System.Data.IsolationLevel;

namespace Dat210.PhotoManager.Data.Indexing.Sql
{
    public class SqlIndexRepository : IIndexRepository
    {
        private readonly DateTime _minDate = new DateTime(1900, 1, 1);
        private static readonly ILog Log = LogManager.GetLogger(typeof(SqlIndexRepository));

        private static IEnumerable<IImage> GetFilteredImages(SqlIndexDataContext context, IImageFilter filter, bool usePaging)
        {
            var fromDate = filter.FromDate.GetValueOrDefault(DateTime.MinValue);
            var toDate = filter.ToDate.GetValueOrDefault(DateTime.MaxValue);
            int page = filter.PageNumber.GetValueOrDefault(1);
            int size = filter.PageSize.GetValueOrDefault(500);
            int minRating = filter.MinimumRating.GetValueOrDefault(0);
            IEnumerable<IImage> images = context.Images.Include("Tags").Where(i =>
                (String.IsNullOrEmpty(filter.Tag) || i.Tags.Any(t => t.Name.Equals(filter.Tag, StringComparison.InvariantCultureIgnoreCase)))
                && (i.Taken >= fromDate)
                && (i.Taken < toDate)
                && (i.Rating >= minRating));
            bool r = filter.Order.HasFlag(ImageOrder.Descending);

            if (filter.Order.HasFlag(ImageOrder.Pixels))
            {
                images = (r ? images.OrderByDescending(i => i.Width * i.Height) : images.OrderBy(i => i.Width * i.Height));
            }
            else if (filter.Order.HasFlag(ImageOrder.Name))
            {
                images = (r ? images.OrderByDescending(i => i.Name) : images.OrderBy(i => i.Name));
            }
            else
            {
                images = (r ? images.OrderByDescending(i => i.Taken) : images.OrderBy(i => i.Taken));
            }
            if (usePaging)
                return images.Skip((page - 1)*size).Take(size);
            return images;
        }

        public void Add(IImage image)
        {
            if (image.Modified < _minDate)
                throw new Exception("Invalid date modified: " + image.Modified);
            if (image.Taken < _minDate)
                throw new Exception("Invalid date taken: " + image.Taken);
            using (var context = new SqlIndexDataContext())
            {
                context.Database.CommandTimeout = 240;
                var dbImage = new Image
                {
                    Created = image.Created,
                    Id = image.Id,
                    Modified = image.Modified,
                    Name = image.Name,
                    OriginalFilePath = image.OriginalFilePath,
                    SmallThumbnailFilePath = image.SmallThumbnailFilePath,
                    PreviewFilePath = image.PreviewFilePath,
                    Rating = image.Rating,
                    Taken = image.Taken,
                    Height = image.Height,
                    Width = image.Width,
                    CameraModel = image.CameraModel,
                    Comment = image.Comment,
                    Hash = image.Hash,
                };
                context.Images.Add(dbImage);

                if (image.Tags != null)
                {
                    foreach (var tag in image.Tags)
                    {
                        var dbTag = context.Tags.SingleOrDefault(t => t.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase));
                        if (dbTag == null)
                        {
                            dbTag = new Tag
                            {
                                Name = tag
                            };
                            context.Tags.Add(dbTag);
                        }
                        dbImage.Tags.Add(dbTag);
                    }
                }
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    throw new Exception("Failed to save changes" +Environment.NewLine + string.Join(Environment.NewLine, ex.EntityValidationErrors.SelectMany(err => err.ValidationErrors.Select(v => v.PropertyName + ": " + v.ErrorMessage))));
                }
                
            }
        }

        public void Save(IImage image)
        {
            if (image.Modified < _minDate)
                throw new Exception("Invalid date modified: " + image.Modified);
            if (image.Taken < _minDate)
                throw new Exception("Invalid date taken: " + image.Taken);
            using (var context = new SqlIndexDataContext())
            {
                var dbImage = context.Images.Include("Tags").Single(i => i.Id == image.Id);
                dbImage.Rating = image.Rating;
                dbImage.Taken = image.Taken;
                dbImage.Name = image.Name;
                dbImage.Hash = image.Hash;
                dbImage.PreviewFilePath = image.PreviewFilePath;
                dbImage.SmallThumbnailFilePath = image.SmallThumbnailFilePath;
                dbImage.Modified = image.Modified;
                dbImage.Width = image.Width;
                dbImage.Height = image.Height;
                dbImage.CameraModel = image.CameraModel;
                dbImage.Comment = image.Comment;
                
                if (image.Tags != null)
                {
                    foreach (var tag in image.Tags
                        .Where(tag => 
                            !String.IsNullOrEmpty(tag) &&
                            dbImage.Tags.All(t => 
                                !string.Equals(tag, t.Name, StringComparison.InvariantCultureIgnoreCase))))
                    {
                        var dbTag = context.Tags.SingleOrDefault(t => t.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase));
                        if (dbTag == null)
                        {
                            dbTag = new Tag
                            {
                                Name = tag
                            };
                            context.Tags.Add(dbTag);
                        }
                        dbImage.Tags.Add(dbTag);
                    }
                    var dbImageTags = dbImage.Tags
                        .Where(tag => image.Tags.All(t => !string.Equals(tag.Name, t, StringComparison.InvariantCultureIgnoreCase))).ToArray();
                    foreach (var tag in dbImageTags)
                    {
                        dbImage.Tags.Remove(tag);
                    }
                    
                }
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    throw new Exception("Failed to save changes" + Environment.NewLine + string.Join(Environment.NewLine, ex.EntityValidationErrors.SelectMany(err => err.ValidationErrors.Select(v => v.PropertyName + ": " + v.ErrorMessage))));
                }
            }
        }

        public void Remove(Guid id)
        {
            using (var context = new SqlIndexDataContext())
            {
                var image = context.Images.Single(i => i.Id == id);
                context.Images.Remove(image);
                context.SaveChanges();
            }
        }

        public IEnumerable<IImage> GetImages(IImageFilter filter)
        {
            List<IImage> images;
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted,
            }))
            {
                using (var context = new SqlIndexDataContext())
                {
                    context.Configuration.AutoDetectChangesEnabled = false;
                    images = GetFilteredImages(context, filter, true).ToList();
                }
                scope.Complete();
            }

            return images;
        }

        public IEnumerable<string> GetAllImagePaths()
        {
            using (var context = new SqlIndexDataContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                return context.Images.Select(i => i.OriginalFilePath).ToArray();
            }
        }

        public IEnumerable<string> GetMostlyUsedTags(int count)
        {
            using (var context = new SqlIndexDataContext())
            {
                return context.Tags.Where(t => t.Images.Any()).OrderByDescending(t => t.Images.Count).Take(count).Select(t => t.Name).ToArray();
            }
        }

        public IImage GetImage(Guid id)
        {
            using (var context = new SqlIndexDataContext())
            {
                var image = context.Images.Include("Tags").FirstOrDefault(i => i.Id == id);
                if (image == null)
                    throw new ImageNotFoundException(id);
                return image;
            }
        }

        private static Guid? GetNeighbourId(Guid current, IImageFilter filter, bool previous)
        {
            using (var context = new SqlIndexDataContext())
            {
                var filteredImages = GetFilteredImages(context, filter, false).Select((image, index) => new{Id = image.Id, Index = index}).ToArray();
                var currentIndex = filteredImages.Single(i => i.Id == current).Index;
                if (previous && currentIndex == 0)
                    return null;
                if (!previous && currentIndex == filteredImages.Count() - 1)
                    return null;
                var neighbour = (previous ? filteredImages.LastOrDefault(o => o.Index < currentIndex) : filteredImages.FirstOrDefault(o => o.Index > currentIndex));
                if (neighbour != null)
                    return neighbour.Id;
                return null;
            }
        }

        public Guid? GetNextImageId(Guid current, IImageFilter filter)
        {
            return GetNeighbourId(current, filter, false);
        }

        public Guid? GetPreviousImageId(Guid current, IImageFilter filter)
        {
            return GetNeighbourId(current, filter, true);
        }

        public IImage GetImage(string originalFilePath)
        {
            using (var context = new SqlIndexDataContext())
            {
                return context.Images.FirstOrDefault(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public bool Contains(string originalFilePath, DateTime lastModified)
        {
            using (var context = new SqlIndexDataContext())
            {
                foreach (var image in context.Images
                    .Where(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase)))
                {
                    if (image.Modified.TimeEquals(lastModified))
                        return true;
                }
            }
            return false;
        }

        public bool Contains(string originalFilePath)
        {
            using (var context = new SqlIndexDataContext())
            {
                return context.Images.Any(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase));
            }
        }
    }
}
