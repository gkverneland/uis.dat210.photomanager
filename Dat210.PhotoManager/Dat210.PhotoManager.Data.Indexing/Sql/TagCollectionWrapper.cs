﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Data.Indexing.Sql.Entities;

namespace Dat210.PhotoManager.Data.Indexing.Sql
{
    public class TagCollectionWrapper : ICollection<string>
    {
        private readonly ICollection<Tag> _tags;

        public TagCollectionWrapper(ICollection<Tag> tags)
        {
            _tags = tags;
        }

        public IEnumerator<string> GetEnumerator()
        {
            return _tags.Select(t => t.Name).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(string item)
        {
            _tags.Add(new Tag{Name = item});
        }

        public void Clear()
        {
            _tags.Clear();
        }

        public bool Contains(string item)
        {
            return _tags.Any(t => t.Name.Equals(item, StringComparison.InvariantCultureIgnoreCase));
        }

        public void CopyTo(string[] array, int arrayIndex)
        {
            Array.Copy(_tags.Select(t => t.Name).ToArray(), 0, array, arrayIndex, _tags.Count);
        }

        public bool Remove(string item)
        {
            var tag = _tags.SingleOrDefault(t => t.Name.Equals(item, StringComparison.InvariantCultureIgnoreCase));
            if (tag != null)
            {
                _tags.Remove(tag);
                return true;
            }
            return false;
        }

        public int Count
        {
            get { return _tags.Count; }
        }

        public bool IsReadOnly
        {
            get { return _tags.IsReadOnly; }
        }
    }
}
