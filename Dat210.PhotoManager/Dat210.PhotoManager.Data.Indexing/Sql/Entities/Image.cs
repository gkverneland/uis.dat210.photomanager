﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Data.Indexing.Sql.Entities
{
    public class Image : IImage
    {
        public Image()
        {
            Tags = new HashSet<Tag>();
        }
        
        [Key]
        public Guid Id { get; set; }
        [MaxLength(255)]
        public string Name { get; set; }
        [MaxLength(1024)]
        public string OriginalFilePath { get; set; }
        [MaxLength(1024)]
        public string Hash { get; set; }
        [MaxLength(1024)]
        public string SmallThumbnailFilePath { get; set; }
        [MaxLength(1024)]
        public string PreviewFilePath { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public int Rating { get; set; }
        public DateTime Taken { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string CameraModel { get; set; }
        public string Comment { get; set; }

        public ICollection<Tag> Tags { get; set; }

        ICollection<string> IImage.Tags
        {
            get { return new TagCollectionWrapper(Tags); }
        }
    }
}
