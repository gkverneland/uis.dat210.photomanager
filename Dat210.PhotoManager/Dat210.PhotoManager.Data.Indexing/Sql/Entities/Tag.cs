﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dat210.PhotoManager.Data.Indexing.Sql.Entities
{
    public class Tag
    {
        public Tag()
        {
// ReSharper disable once DoNotCallOverridableMethodsInConstructor
            Images = new HashSet<Image>();
        }

        [Key]
        public string Name { get; set; }

        public virtual ICollection<Image> Images { get; set; }
    }
}