﻿using System.Data.Entity;
using Dat210.PhotoManager.Data.Indexing.Migrations;
using Dat210.PhotoManager.Data.Indexing.Sql.Entities;

namespace Dat210.PhotoManager.Data.Indexing.Sql
{
    public class SqlIndexDataContext : DbContext
    {
        public SqlIndexDataContext() : base("connectionstring")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SqlIndexDataContext, Configuration>()); 
        }

        public DbSet<Image> Images { get; set; }

        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Image>().
              HasMany(c => c.Tags).
              WithMany(p => p.Images).
              Map(
               m =>
               {
                   m.MapLeftKey("ImageId");
                   m.MapRightKey("TagName");
                   m.ToTable("ImageTags");
               });
        }
    }
}
