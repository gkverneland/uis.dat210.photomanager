using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Library;

namespace Dat210.PhotoManager.Data.Indexing.Sql
{
    /// <summary>
    /// NOTE: Tags don't work properly when using this repository
    /// </summary>
    public class CachedSqlIndexRepository : IIndexRepository
    {
        private readonly SqlIndexRepository _sql;
        private readonly ConcurrentDictionary<Guid, IImage> _index;
        public CachedSqlIndexRepository()
        {
            _sql = new SqlIndexRepository();
            _index = new ConcurrentDictionary<Guid, IImage>();
        }

        public void Add(IImage image)
        {
            if (image == null) throw new ArgumentNullException("image");
            _sql.Add(image);
            _index[image.Id] = image;
        }

        public void Save(IImage image)
        {
            _sql.Save(image);
            _index[image.Id] = image;
        }

        public void Remove(Guid id)
        {
            _sql.Remove(id);
            IImage removed;
            if (!_index.TryRemove(id, out removed))
                throw new Exception("Failed to remove image " + id);
        }

        public IEnumerable<IImage> GetImages(IImageFilter filter)
        {
            foreach (var image in _sql.GetImages(filter))
            {
                _index[image.Id] = image;
                yield return image;
            }
        }

        public IEnumerable<string> GetAllImagePaths()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetMostlyUsedTags(int count)
        {
            throw new NotImplementedException();
        }

        public IImage GetImage(Guid id)
        {
            if (!_index.ContainsKey(id))
            {
                _index[id] = _sql.GetImage(id);
            }
            return _index[id];
        }

        public Guid? GetNextImageId(Guid current, IImageFilter filter)
        {
            return _sql.GetNextImageId(current, filter);
        }

        public Guid? GetPreviousImageId(Guid current, IImageFilter filter)
        {
            return _sql.GetPreviousImageId(current, filter);
        }

        public IImage GetImage(string originalFilePath)
        {
            var indexedImage = _index.Values.FirstOrDefault(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase));
            if (indexedImage != null)
                return indexedImage;
            var sqlImage = _sql.GetImage(originalFilePath);
            if (sqlImage != null)
                _index[sqlImage.Id] = sqlImage;
            return sqlImage;
        }

        public bool Contains(string originalFilePath, DateTime lastModified)
        {
            if (_index.Values.Any(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase) && i.Modified.TimeEquals(lastModified)))
                return true;
            return _sql.Contains(originalFilePath, lastModified);
        }

        public bool Contains(string originalFilePath)
        {
            if (_index.Values.Any(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase)))
                return true;
            return _sql.Contains(originalFilePath);
        }
    }
}