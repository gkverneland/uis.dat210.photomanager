﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Library;

namespace Dat210.PhotoManager.Data.Indexing
{
    public class InMemoryIndex : IIndexRepository
    {
        public InMemoryIndex()
        {
#if DEBUG
            Console.WriteLine("Initializing " + GetType().Name);
#endif
            _index = new ConcurrentDictionary<Guid, IImage>();
        }

        private readonly ConcurrentDictionary<Guid, IImage> _index;

        public void Add(IImage image)
        {
            if (image == null) throw new ArgumentNullException("image");
            _index[image.Id] = image;
        }

        public void Save(IImage image)
        {
            _index[image.Id] = image;
        }

        public IEnumerable<IImage> GetAllImages()
        {
            return _index.Values.ToArray();
        }

        public IEnumerable<string> GetAllImagePaths()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetMostlyUsedTags(int count)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<IImage> GetFilteredImages(IImageFilter filter, bool usePaging)
        {
            var fromDate = filter.FromDate.GetValueOrDefault(DateTime.MinValue);
            var toDate = filter.ToDate.GetValueOrDefault(DateTime.MaxValue);
            int page = filter.PageNumber.GetValueOrDefault(1);
            int size = filter.PageSize.GetValueOrDefault(500);
            int minRating = filter.MinimumRating.GetValueOrDefault(0);
            IEnumerable<IImage> images = _index.Values.Where(i =>
                (String.IsNullOrEmpty(filter.Tag) || i.Tags.Any(t => t.Equals(filter.Tag, StringComparison.InvariantCultureIgnoreCase)))
                && (i.Taken >= fromDate)
                && (i.Taken < toDate)
                && (i.Rating >= minRating));
            bool r = filter.Order.HasFlag(ImageOrder.Descending);

            if (filter.Order.HasFlag(ImageOrder.Pixels))
            {
                images = (r ? images.OrderByDescending(i => i.Width * i.Height) : images.OrderBy(i => i.Width * i.Height));
            }
            else if (filter.Order.HasFlag(ImageOrder.Name))
            {
                images = (r ? images.OrderByDescending(i => i.Name) : images.OrderBy(i => i.Name));
            }
            else
            {
                images = (r ? images.OrderByDescending(i => i.Taken) : images.OrderBy(i => i.Taken));
            }
            if (usePaging)
                return images.Skip((page - 1) * size).Take(size);
            return images;
        }

        public IEnumerable<IImage> GetImages(IImageFilter filter)
        {
            return GetFilteredImages(filter, true).ToList();
        }

        public IImage GetImage(Guid id)
        {
            return _index[id];
        }

        public Guid? GetNextImageId(Guid current, IImageFilter filter)
        {
            throw new NotImplementedException();
        }

        public Guid? GetPreviousImageId(Guid current, IImageFilter filter)
        {
            throw new NotImplementedException();
        }

        public IImage GetImage(string originalFilePath)
        {
            return _index.Values.SingleOrDefault(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool Contains(string originalFilePath, DateTime lastModified)
        {
            return _index.Values.Any(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase) && i.Modified.TimeEquals(lastModified));
        }

        public bool Contains(string originalFilePath)
        {
            return _index.Values.Any(i => i.OriginalFilePath.Equals(originalFilePath, StringComparison.InvariantCultureIgnoreCase));
        }

        public void Remove(Guid id)
        {
            IImage removed;
            if (!_index.TryRemove(id, out removed))
                throw new Exception("Failed to remove image " + id);
        }
    }
}
