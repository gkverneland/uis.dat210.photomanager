﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dat210.PhotoManager.Data.Indexing.Exceptions
{
    public class ImageNotFoundException : Exception
    {
        public ImageNotFoundException(Guid id) 
            : base("Image with id " + id + " was not found")
        {
            
        }
    }
}
