﻿using Dat210.PhotoManager.Data.Indexing.Sql;
using Ninject.Modules;

namespace Dat210.PhotoManager.Data.Indexing
{
    public class DependencyBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IIndexRepository>().To<SqlIndexRepository>().InSingletonScope();
        }
    }
}
