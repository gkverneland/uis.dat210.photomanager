﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Library;
using Fotofly;
using log4net;

namespace Dat210.PhotoManager.Business.Fotofly
{
    public class FotoflyImageMetadataWrapper : IMetadataWrapper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(FotoflyImageMetadataWrapper));
        private readonly JpgPhoto _jpg;
        private bool _dirty;

        public FotoflyImageMetadataWrapper(string path)
        {
            _jpg = new JpgPhoto(path);
            if (!_jpg.FileExists)
                throw new FileNotFoundException("Could not load data for non-existing image file", path);
            _jpg.ReadMetadata();
        }

        public DateTime Taken
        {
            get
            {
                try
                {
                    return _jpg.Metadata.DateTaken;
                }
                catch (NotImplementedException) //time zone is not implemented - just ignore it for now
                {
                    return DateTime.MinValue;
                }
                catch (Exception ex)
                {
                    if (ex.InnerException is NotImplementedException)
                        return DateTime.MinValue;
                    throw;
                }
            }
            set
            {
                if (!_jpg.Metadata.DateTaken.TimeEquals(value))
                {
                    _jpg.Metadata.DateTaken = value;
                    OnChange();
                }
            }
        }

        public string CameraModel
        {
            get { return _jpg.Metadata.CameraModel; }
        }

        public string Commment
        {
            get { return _jpg.Metadata.Comment; }
            set
            {
                if (!String.Equals(value, _jpg.Metadata.Comment))
                {
                    _jpg.Metadata.Comment = value;
                    OnChange();
                }
            }
        }

        public int Rating
        {
            get
            {
                if (_jpg.Metadata.Rating == null)
                    return 0;
                return Convert.ToInt32(_jpg.Metadata.Rating.Numerical);
            }
            set
            {
                if (Convert.ToInt32(_jpg.Metadata.Rating.Numerical) != value)
                {
                    _jpg.Metadata.Rating = new Rating(value);
                    OnChange();
                }
            }
        }

        public int Width
        {
            get { return _jpg.Metadata.ImageWidth; }
        }

        public int Height
        {
            get { return _jpg.Metadata.ImageHeight; }
        }

        public IEnumerable<string> Tags
        {
            get { return _jpg.Metadata.Tags.Select(t => t.Name).ToArray(); }
            set
            {
                if (value.Count() != _jpg.Metadata.Tags.Count
                    || _jpg.Metadata.Tags.Any(t => value.All(v => !String.Equals(v, t.Name, StringComparison.InvariantCultureIgnoreCase)))
                    || value.Any(v => _jpg.Metadata.Tags.All(
                                t => !String.Equals(t.Name, v, StringComparison.InvariantCultureIgnoreCase))))
                {
                    _dirty = true;
                    _jpg.Metadata.Tags = new TagList(value.ToArray());
                }
            }
        }

        public void Save()
        {
            if (_dirty)
            {
                _jpg.WriteMetadata();
                Log.Debug("Metadata saved: " + _jpg.FileFullName);
                _dirty = false;
            }
        }

        public void Dispose()
        {
            Save();
        }

        public void OnChange()
        {
            _dirty = true;
        }
    }
}