﻿using System;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;

namespace Dat210.PhotoManager.Business.Fotofly
{
    public class FotoflyMetadataProvider : IMetadataProvider
    {
        private readonly IFileRepository _fileRepository;

        public FotoflyMetadataProvider(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        public IMetadataWrapper GetMetadataWrapper(string path)
        {
            lock (_fileRepository.EnsureFileIsAccessible(path, TimeSpan.FromMinutes(1)))
                return new FotoflyImageMetadataWrapper(path);
        }
    }
}
