﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Business
{
    public class ApplicationConfiguration : IApplicationConfiguration
    {
        private string _photoLibraryPath;
        public string GetPhotoLibraryPath()
        {
            if (_photoLibraryPath == null)
            {
                _photoLibraryPath = ConfigurationManager.AppSettings["PhotoLibraryPath"];
                if (String.IsNullOrEmpty(_photoLibraryPath))
                    throw new ConfigurationErrorsException("Photo Library path not defined");
            }
            return _photoLibraryPath;
        }

        private string _thumbnailPath;
        public string GetThumbnailPath()
        {
            if (_thumbnailPath == null)
            {
                _thumbnailPath = ConfigurationManager.AppSettings["ThumbnailPath"];
                if (String.IsNullOrEmpty(_thumbnailPath))
                    _thumbnailPath = Path.Combine(Path.GetTempPath(), "Uis.Dat210.PhotoManager");
            }
            return _thumbnailPath;
        }

        public void SetPhotoLibraryPath(string value)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("/");
            if (!config.AppSettings.Settings.AllKeys.Contains("PhotoLibraryPath"))
            {
                config.AppSettings.Settings.Add("PhotoLibraryPath", value);
            }
            else
            {
                config.AppSettings.Settings["PhotoLibraryPath"].Value = value;
            }
            config.Save(ConfigurationSaveMode.Modified);
            _photoLibraryPath = value;
        }

        public void SetThumbnailPath(string value)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("/");
            if (!config.AppSettings.Settings.AllKeys.Contains("ThumbnailPath"))
            {
                config.AppSettings.Settings.Add("ThumbnailPath", value);
            }
            else
            {
                config.AppSettings.Settings["ThumbnailPath"].Value = value;
            }
            
            config.Save(ConfigurationSaveMode.Modified);
            _photoLibraryPath = value;
        }
    }
}
