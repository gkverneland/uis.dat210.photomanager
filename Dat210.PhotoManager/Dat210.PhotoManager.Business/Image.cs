﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;

namespace Dat210.PhotoManager.Business
{
    public class Image : IImage
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string OriginalFilePath { get; set; }
        public string Hash { get; set; }
        public string SmallThumbnailFilePath { get; set; }
        public string PreviewFilePath { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Taken { get; set; }
        public int Rating { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string CameraModel { get; set; }
        public string Comment { get; set; }
        public ICollection<string> Tags { get; set; }
    }
}
