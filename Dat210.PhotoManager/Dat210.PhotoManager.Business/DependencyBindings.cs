﻿using Dat210.PhotoManager.Business.Fotofly;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Data.Indexing;
using log4net;
using Ninject.Modules;

namespace Dat210.PhotoManager.Business
{
    public class DependencyBindings : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new[]
            {
                typeof (IFileRepository).Assembly,
                typeof (IIndexRepository).Assembly
            });

            Bind<IIndexingService>().To<IndexingService>().InSingletonScope();
            Bind<IApplicationConfiguration>().To<ApplicationConfiguration>().InSingletonScope();
            Bind<IImageLibrary>().To<ImageLibrary>();
            Bind<IImageToolbox>().To<ImageToolbox>();
            Bind<IMetadataProvider>().To<FotoflyMetadataProvider>();
        }
    }
}
