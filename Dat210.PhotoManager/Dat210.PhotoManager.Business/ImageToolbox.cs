﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;
using Dat210.PhotoManager.Contracts;
using Sherwood.Imaging;
using Sherwood.Imaging.Behaviors;
using Sherwood.Imaging.Filters;
using Sherwood.Imaging.Providers;

namespace Dat210.PhotoManager.Business
{
    public class ImageToolbox : IImageToolbox
    {
        public byte[] ResizeImage(byte[] image, int maxWidth, int maxHeight)
        {
            if (image == null || image.Length == 0) throw new ArgumentNullException("image");
            if (maxWidth > 10000)
                maxWidth = 10000;
            if (maxHeight > 10000)
                maxHeight = 10000;
            var stream = new MemoryStream(image);
            var img = System.Drawing.Image.FromStream(stream);

            var template = new Template("Resize_" + maxHeight + "_" + maxWidth, new StaticImageProvider(img));
            template.Filters.Add(new ChangePixelFormatFilter(PixelFormat.Format32bppRgb));
            template.Filters.Add(new ScaleImageFilter(maxWidth, maxHeight, new SimpleResizeBehavior()));

            template.SaveToStream(stream);
            using (var memoryStream = new MemoryStream())
            {
                template.SaveToStream(memoryStream);
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public byte[] RotateImage(byte[] image, bool counterClockWise)
        {
            System.Drawing.Image img;
            using (var ms = new MemoryStream(image))
                img = System.Drawing.Image.FromStream(ms);
            img.RotateFlip(counterClockWise ? RotateFlipType.Rotate270FlipNone : RotateFlipType.Rotate90FlipNone);
            using (var outputStream = new MemoryStream())
            {
                img.Save(outputStream, ImageFormat.Jpeg);
                return outputStream.ToArray();
            }
        }
    }
}
