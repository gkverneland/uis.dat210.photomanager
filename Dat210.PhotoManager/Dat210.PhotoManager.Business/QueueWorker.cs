﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace Dat210.PhotoManager.Business
{
    public class QueueWorker : IDisposable
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(QueueWorker));
        private readonly AutoResetEvent _waitHandle = new AutoResetEvent(false);
        private static Thread _workerThread;
        public long size;
        public QueueWorker()
        {
            _queue = new Queue<QueueItem>();
            if (_workerThread != null)
                throw new InvalidOperationException("There can only exist one instance of this class");
            _workerThread = new Thread(Work);
            _workerThread.Start();
            
        }

        private readonly Queue<QueueItem> _queue;

        public void Work()
        {
            Thread.Sleep(TimeSpan.FromSeconds(10));
            while (true)
            {
                try
                {
                    while (_queue.Any())
                    {
                        _queue.Dequeue().Action.Invoke();
                        size--;
                    }
                    _waitHandle.WaitOne();
                }
                catch (ThreadAbortException)
                {
                    
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
        }

        public void Add(Action action)
        {
            _queue.Enqueue(new QueueItem(action));
            _waitHandle.Set();
            size++;
            Log.Debug("QueueWorker has " + size + " tasks");
        }

        public DateTime GetTime()
        {
            try
            {
                return _queue.Peek().Date;
            }
            catch (InvalidOperationException)
            {
                return DateTime.Now;
            }
        }

        public void Dispose()
        {
            if (_workerThread != null)
                _workerThread.Abort();
            _workerThread = null;
        }
    }
}
