﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Data.Indexing;
using log4net;

namespace Dat210.PhotoManager.Business
{
    public class ImageLibrary : IImageLibrary
    {
        private readonly IIndexRepository _indexRepository;
        private readonly IFileRepository _fileRepository;
        private readonly IImageToolbox _toolbox;
        private readonly IMetadataProvider _metadataProvider;
        private static readonly ILog Log = LogManager.GetLogger(typeof(ImageLibrary));

        public ImageLibrary(IIndexRepository indexRepository, IFileRepository fileRepository, IImageToolbox toolbox, IMetadataProvider metadataProvider)
        {
            _indexRepository = indexRepository;
            _fileRepository = fileRepository;
            _toolbox = toolbox;
            _metadataProvider = metadataProvider;
        }

        public IEnumerable<IImage> GetImageInfo(IImageFilter filter)
        {
            return _indexRepository.GetImages(filter);
        }

        public IImage GetImageInfo(Guid id)
        {
            return _indexRepository.GetImage(id);
        }

        public IFile GetSmallThumbnail(Guid id)
        {
            var image = _indexRepository.GetImage(id);

            return _fileRepository.ReadImageFile(image.SmallThumbnailFilePath);
        }

        public IFile GetPreview(Guid id)
        {
            var image = _indexRepository.GetImage(id);

            return _fileRepository.ReadImageFile(image.PreviewFilePath);
        }

        public IFile GetImage(Guid id)
        {
            var image = _indexRepository.GetImage(id);
            return _fileRepository.ReadImageFile(image.OriginalFilePath);
        }

        public IEnumerable<string> GetMostlyUsedTags(int count)
        {
            return _indexRepository.GetMostlyUsedTags(count);
        }

        public Guid? GetNextImageId(Guid current, IImageFilter filter)
        {
            return _indexRepository.GetNextImageId(current, filter);
        }

        public Guid? GetPreviousImageId(Guid current, IImageFilter filter)
        {
            return _indexRepository.GetPreviousImageId(current, filter);
        }

        public string CreatePreview(byte[] image, string extension)
        {
            return CreateThumbnail(image, 1600, 900, extension);
        }

        public string CreateSmallThumbnail(byte[] image, string extension)
        {
            return CreateThumbnail(image, 120, 90, extension);
        }

        private string CreateThumbnail(byte[] image, int maxWidth, int maxHeight, string extension)
        {
            var thumbnail = _toolbox.ResizeImage(image, maxWidth, maxHeight);
            return _fileRepository.SaveThumbnail(thumbnail, extension);
        }

        public void AddImage(string path)
        {
            lock (_fileRepository.EnsureFileIsAccessible(path, TimeSpan.FromMinutes(3)))
            {
                var fileInfo = _fileRepository.GetImageFile(path);
                Log.Info("Adding image " + fileInfo.Path);
                var file = _fileRepository.ReadImageFile(fileInfo.Path);
                IMetadataWrapper metadata = null;
                try
                {
                    metadata = _metadataProvider.GetMetadataWrapper(fileInfo.Path);
                }
                catch (Exception ex)
                {
                    if (!(ex.InnerException is NotImplementedException) && !(ex.InnerException is NotSupportedException))
                    {
                        throw new Exception("Failed to read metadata (" + ex.GetType().Name + ")", ex);
                    }
                }
                
                _indexRepository.Add(new Image
                {
                    OriginalFilePath = fileInfo.Path,
                    Id = Guid.NewGuid(),
                    SmallThumbnailFilePath = CreateSmallThumbnail(file.Data, fileInfo.Extension),
                    Name = fileInfo.Name,
                    Created = fileInfo.Created,
                    Modified = fileInfo.Modified,
                    PreviewFilePath = CreatePreview(file.Data, fileInfo.Extension),
                    Rating = (metadata != null ? metadata.Rating : 0),
                    Width = (metadata != null ? metadata.Width : 0),
                    Height = (metadata != null ? metadata.Height : 0),
                    Taken = (metadata == null || metadata.Taken == DateTime.MinValue ? fileInfo.Created : metadata.Taken),
                    CameraModel = (metadata != null ? metadata.CameraModel : null),
                    Comment = (metadata != null ? metadata.Commment : null),
                    Tags = (metadata != null ? metadata.Tags.ToArray() : null),
                    Hash = _fileRepository.GetFileHash(file.Data)
                });
            }
            
        }

        public void UpdateImage(string path)
        {
            HashSet<string> filesToDeleteAfterSave = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            lock (_fileRepository.EnsureFileIsAccessible(path, TimeSpan.FromMinutes(3)))
            {
                Log.Info("Updating " + path);
                var fileInfo = _fileRepository.GetImageFile(path);
                var file = _fileRepository.ReadImageFile(path);
                var newHash = _fileRepository.GetFileHash(file.Data);
                var existingImage = _indexRepository.GetImage(path);
                if (newHash != existingImage.Hash)
                {
                    Log.Info("Image has changed (" + newHash + " vs " + existingImage.Hash + ")");
                    filesToDeleteAfterSave.Add(existingImage.PreviewFilePath);
                    filesToDeleteAfterSave.Add(existingImage.SmallThumbnailFilePath);
                    existingImage.PreviewFilePath = CreatePreview(file.Data, fileInfo.Extension);
                    existingImage.SmallThumbnailFilePath = CreateSmallThumbnail(file.Data, fileInfo.Extension);
                    existingImage.Hash = newHash;
                }
                existingImage.Modified = fileInfo.Modified;
                var metadata = _metadataProvider.GetMetadataWrapper(path);
                existingImage.Rating = metadata.Rating;
                existingImage.Width = metadata.Width;
                existingImage.Height = metadata.Height;
                existingImage.Taken = (metadata.Taken == DateTime.MinValue ? fileInfo.Created : metadata.Taken);
                existingImage.CameraModel = metadata.CameraModel;
                existingImage.Comment = metadata.Commment;
                    
                foreach (var tag in existingImage.Tags)
                {
                    if (metadata.Tags.All(t => !t.Equals(tag, StringComparison.InvariantCultureIgnoreCase)))
                        existingImage.Tags.Remove(tag);
                }
                foreach (var tag in metadata.Tags)
                {
                    if (existingImage.Tags.All(t => !t.Equals(tag, StringComparison.InvariantCultureIgnoreCase)))
                        existingImage.Tags.Add(tag);
                }
                _indexRepository.Save(existingImage);
            }
            foreach (var file in filesToDeleteAfterSave)
                _fileRepository.Delete(file);
        }
        /// <summary>
        /// Used to save image to disk and index when changed e.g. via GUI
        /// </summary>
        /// <param name="image"></param>
        public void SaveImage(IImage image)
        {
            lock (_fileRepository.EnsureFileIsAccessible(image.OriginalFilePath, TimeSpan.FromMinutes(2)))
            {
                image.Modified = DateTime.Now;
                _indexRepository.Save(image);

                using (var metadata = _metadataProvider.GetMetadataWrapper(image.OriginalFilePath))
                {
                    metadata.Rating = image.Rating;
                    metadata.Commment = image.Comment ?? "";
                    metadata.Tags = image.Tags;
                    metadata.Taken = image.Taken;

                    metadata.Save();
                }
            }
        }

        public void RotateImage(Guid id, bool counterClockwise)
        {
            var image = _indexRepository.GetImage(id);

            RotateImage(image.SmallThumbnailFilePath, counterClockwise);
            RotateImage(image.PreviewFilePath, counterClockwise);

            int height = image.Height;
            int width = image.Width;
            image.Height = width;
            image.Width = height;
            byte[] rotatedImage;
            lock (_fileRepository.EnsureFileIsAccessible(image.OriginalFilePath, TimeSpan.FromMinutes(2)))
            {
                rotatedImage = _toolbox.RotateImage(_fileRepository.ReadImageFile(image.OriginalFilePath).Data,
                    counterClockwise);
                image.Hash = _fileRepository.GetFileHash(rotatedImage);
            }
            
            image.Modified = DateTime.Now;
            _indexRepository.Save(image);
            _fileRepository.SaveImage(rotatedImage, image.OriginalFilePath);
        }

        public void DeleteImage(Guid id)
        {
            var image = _indexRepository.GetImage(id);
            if (image != null)
            {
                HashSet<string> filesToRemove = new HashSet<string>
                {
                    image.OriginalFilePath,
                    image.PreviewFilePath,
                    image.SmallThumbnailFilePath
                };
                _indexRepository.Remove(id);

                foreach (var file in filesToRemove)
                {
                    if (_fileRepository.FileExists(file))
                    {
                        lock (_fileRepository.EnsureFileIsAccessible(file, TimeSpan.FromMinutes(3)))
                        {
                            _fileRepository.Delete(file);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Rotates an image and returns file hash
        /// </summary>
        private void RotateImage(string path, bool counterClockwise)
        {
            lock (_fileRepository.EnsureFileIsAccessible(path, TimeSpan.FromMinutes(2)))
            {
                var newImage = _toolbox.RotateImage(_fileRepository.ReadImageFile(path).Data, counterClockwise);
                _fileRepository.SaveImage(newImage, path);
            }
        }
    }
}
