﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dat210.FileWatcher;
using log4net;

namespace Dat210.PhotoManager.Business
{
    public class FileChangeWatcher : IDisposable
    {
        private readonly Action<FileReadyEventArgs> _onFileChanged;
        private static readonly ILog Log = LogManager.GetLogger(typeof(FileChangeWatcher));
        private static readonly TimeSpan Timeout = TimeSpan.FromMinutes(10);
        private FileGrabber _grabber;

        public FileChangeWatcher(string folder, string fileFilter, Action<FileReadyEventArgs> onFileChanged)
        {
            _onFileChanged = onFileChanged;
            Folder = folder;
            FileFilter = fileFilter;
        }

        public string Folder { get; set; }
        public string FileFilter { get; set; }

        public void Start()
        {
            _grabber = new FileGrabber(Folder, FileFilter, Timeout);
            _grabber.FileReady += GrabberOnFileReady;
            _grabber.FileFailed += GrabberOnFileFailed;
            _grabber.IsEnabled = true;
        }

        private void GrabberOnFileFailed(object sender, FileFailedEventArgs fileFailedEventArgs)
        {
            FileFailed(fileFailedEventArgs.FileName);
        }

        private void GrabberOnFileReady(object sender, FileReadyEventArgs fileReadyEventArgs)
        {
            ProcessFile(fileReadyEventArgs);
        }

        public void ProcessFile(FileReadyEventArgs arguments)
        {
            _onFileChanged(arguments);
        }

        public void FileFailed(string fileName)
        {
            Log.Warn("Failed to read file " + fileName + " (timeout after " + Timeout.TotalMinutes + " minutes)");
        }

        public void Dispose()
        {
            if (_grabber != null)
            {
                _grabber.Dispose();
                _grabber = null;
            }
        }
    }
}
