﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI;
using Dat210.PhotoManager.Contracts;
using Dat210.PhotoManager.Data.FileRepository;
using Dat210.PhotoManager.Data.Indexing;
using Dat210.PhotoManager.Library;
using log4net;

namespace Dat210.PhotoManager.Business
{
    public class IndexingService : IIndexingService, IDisposable
    {
        private readonly IFileRepository _fileRepository;
        private readonly IIndexRepository _indexRepository;
        private readonly IApplicationConfiguration _config;
        private readonly IImageLibrary _imageLibrary;

        private static readonly ILog Log = LogManager.GetLogger(typeof(IndexingService));

        private DateTime _lastIndexing;
        private string _status;
        private FileChangeWatcher _watcher;
        private QueueWorker _queueWorker;
        private static bool _initialized;
        private readonly DateTime _timeStarted;

        public IndexingService(IFileRepository fileRepository, IIndexRepository indexRepository, IApplicationConfiguration config, IImageLibrary imageLibrary)
        {
            if (_initialized) throw new Exception("There can only exist one instance of IndexingService");
            if (fileRepository == null) throw new ArgumentNullException("fileRepository");
            log4net.Config.XmlConfigurator.Configure();
            SetStatus("Initializing");
            _fileRepository = fileRepository;
            _indexRepository = indexRepository;
            _config = config;
            _imageLibrary = imageLibrary;
            _queueWorker = new QueueWorker();
            _watcher = new FileChangeWatcher(_config.GetPhotoLibraryPath(), "*.jpg", f =>
            {
                Log.Info("File " + f.Change + " " + f.FileName);
                if (f.Change == WatcherChangeTypes.Deleted)
                {
                    PerformRemove(f.FileName);
                }
                else if (f.Change == WatcherChangeTypes.Renamed)
                {
                    PerformAdd(f.FileName);
                    PerformRemove();
                }
                else
                {
                    PerformAdd(f.FileName);
                }
            });
            _watcher.Start();

            ThreadPool.QueueUserWorkItem(AddAndRemove);
            SetStatus("Initialized");
            _timeStarted = DateTime.Now;
            _initialized = true;
        }

        public void Dispose()
        {
            SetStatus("Disposed");
            if (_queueWorker != null)
                _queueWorker.Dispose();
            _queueWorker = null;
            _initialized = false;
            _watcher.Dispose();
            _watcher = null;
        }

        private readonly List<Exception> _exceptions = new List<Exception>();
        public IEnumerable<Exception> GetExceptions()
        {
            return _exceptions.ToArray();
        }

        public void Add()
        {
            PerformAdd();
        }

        private void AddAndRemove(object state)
        {
            PerformAdd();
            PerformRemove();
        }
        
        private void PerformAdd()
        {
            SetStatus("Running indexing");
            try
            {
                foreach (var file in _fileRepository.ListImageFiles(_config.GetPhotoLibraryPath(), true))
                {
                    _queueWorker.Add(() => PerformAddInner(file));
                }
                _lastIndexing = DateTime.Now;
                SetStatus("Finished indexing");
            }
            catch (Exception ex)
            {
                SetStatus("Failed to run indexing", ex);
            }
        }

        private void PerformAdd(string path)
        {
            try
            {
                _queueWorker.Add(() => PerformAddInner(path));
            }
            catch (Exception ex)
            {
                SetStatus("Failed to add/update image " + path, ex);
            }
            
        }

        private bool _adding;
        private void PerformAddInner(string path)
        {
            Log.Debug("PerformAddInner " + path);
            if (_adding)
                throw new Exception("This method can be called by a single thread only");
            _adding = true;
            try
            {
                //add non-existing or removed images
                if (_fileRepository.FileExists(path))
                {
                    var file = _fileRepository.GetImageFile(path);
                    if (!_indexRepository.Contains(file.Path, file.Modified))
                    {
                        //get existing image to update if it already exists

                        if (!_indexRepository.Contains(file.Path))
                        {
                            _imageLibrary.AddImage(file.Path);
                            SetStatus("Indexed file " + file.Path);
                        }
                        else
                        {
                            _imageLibrary.UpdateImage(file.Path);
                            Log.Info("Updated image " + file.Name + " (was modified " + file.Modified + ")");
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                SetStatus("Failed to index image " + path, ex);
            }
            finally
            {
                _adding = false;
            }
        }

        public void Remove()
        {
            PerformRemove();
        }
        
        private void PerformRemove()
        {
            SetStatus("Removing deleted images");
            try
            {
                foreach (var file in _indexRepository.GetAllImagePaths().Where(f => !_fileRepository.FileExists(f)))
                {
                    _queueWorker.Add(() => PerformRemoveInner(file));
                }
                SetStatus("Finished removing deleted images");
            }
            catch (Exception ex)
            {
                SetStatus("Failed to remove deleted images", ex);
            }
        }

        private void PerformRemove(string path)
        {
            try
            {
                _queueWorker.Add(() => PerformRemoveInner(path));
            }
            catch (Exception ex)
            {
                SetStatus("Failed to remove image " + path, ex);
            }
        }

        private bool _removing;
        private void PerformRemoveInner(string path)
        {
            Log.Debug("PerformRemoveInner " + path);
            if (_removing)
                throw new Exception("This method can only be run by one thread at a time");
            _removing = true;
            try
            {
                lock (_fileRepository.EnsureFileIsAccessible(path, TimeSpan.FromMinutes(2)))
                {
                    try
                    {
                        if (!_fileRepository.FileExists(path))
                        {
                            var image = _indexRepository.GetImage(path);
                            if (image != null)
                            {
                                _imageLibrary.DeleteImage(image.Id);
                                SetStatus("Removed deleted file " + path);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SetStatus("Failed to remove image " + path, ex);
                    }
                }
            }
            finally
            {
                _removing = false;
            }

        }

        public string GetStatus()
        {
            return _status;
        }

        public DateTime GetLastRun()
        {
            return _lastIndexing;
        }

        public void WaitUntilIndexed()
        {
            DateTime at = DateTime.Now;
            while (_queueWorker.GetTime() < at)
                Thread.Sleep(1000);
        }

        public DateTime GetTimeStarted()
        {
            return _timeStarted;
        }

        public void SetStatus(string status, Exception ex = null)
        {
            _status = status + " (at " + DateTime.Now + ")";
            if (ex == null)
                Log.Info("IndexingService: " + status);
            else
            {
                Log.Error("IndexingService: " + status, ex);
                _exceptions.Add(new Exception(status, ex));
            }
                
        }
    }
}
