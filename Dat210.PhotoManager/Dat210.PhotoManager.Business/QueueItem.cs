﻿using System;

namespace Dat210.PhotoManager.Business
{
    public class QueueItem
    {
        public QueueItem(Action action)
        {
            Action = action;
            Date = DateTime.Now;
        }

        public DateTime Date { get; private set; }

        public Action Action { get; private set; }
    }
}